(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["detail-simplevisitor-detail-simplevisitor-module"],{

/***/ "./src/app/detail-simplevisitor/detail-simplevisitor.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/detail-simplevisitor/detail-simplevisitor.module.ts ***!
  \*********************************************************************/
/*! exports provided: DetailSimplevisitorPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailSimplevisitorPageModule", function() { return DetailSimplevisitorPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _detail_simplevisitor_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detail-simplevisitor.page */ "./src/app/detail-simplevisitor/detail-simplevisitor.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _detail_simplevisitor_page__WEBPACK_IMPORTED_MODULE_5__["DetailSimplevisitorPage"]
    }
];
var DetailSimplevisitorPageModule = /** @class */ (function () {
    function DetailSimplevisitorPageModule() {
    }
    DetailSimplevisitorPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_detail_simplevisitor_page__WEBPACK_IMPORTED_MODULE_5__["DetailSimplevisitorPage"]]
        })
    ], DetailSimplevisitorPageModule);
    return DetailSimplevisitorPageModule;
}());



/***/ }),

/***/ "./src/app/detail-simplevisitor/detail-simplevisitor.page.html":
/*!*********************************************************************!*\
  !*** ./src/app/detail-simplevisitor/detail-simplevisitor.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/listsimplevisitor\" float-left></ion-back-button>\n    </ion-buttons>\n    <ion-title>Visitor Detail</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-card>\n    <ion-card-content>\n      <img class=\"sqr-pic\" [src]=\"this.webview.convertFileSrc(Visitordata.Picture)\" />\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label>Name : </ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item>\n              <span class=\"label_color\">{{Visitordata.Name}}</span>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label>Flat No : </ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item>\n              <span class=\"label_color\">{{Visitordata.BuildingBlock }}-{{Visitordata.FlatNo}}</span>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n\n\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label>Contact No :</ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item>\n              <span class=\"label_color\">{{Visitordata.ContactNo}}</span>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n\n\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label>Vehicle No : </ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item>\n              <span class=\"label_color\">{{Visitordata.VehicleNo}}</span>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-label>Reason : </ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item>\n              <span class=\"label_color\">{{Visitordata.Reason}}</span>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n\n      <!-- <ion-item>\n        <ion-label>FlatNo : <span class=\"label_color\">{{Visitordata.BuildingBlock }}-{{Visitordata.FlatNo}}</span></ion-label>\n      </ion-item>\n      <ion-item>\n        <ion-label>ContactNo : <span class=\"label_color\">{{Visitordata.ContactNo}}</span></ion-label>\n      </ion-item>\n      <ion-item>\n        <ion-label>VehicleNo : <span class=\"label_color\">{{Visitordata.VehicleNo}}</span></ion-label>\n      </ion-item>\n      <ion-item>\n        <ion-label style=\"white-space: normal\">Reason : <span class=\"label_color\">{{Visitordata.Reason}}</span></ion-label>\n      </ion-item> -->\n    </ion-card-content>\n  </ion-card>\n\n\n</ion-content>"

/***/ }),

/***/ "./src/app/detail-simplevisitor/detail-simplevisitor.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/detail-simplevisitor/detail-simplevisitor.page.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sqr-pic {\n  width: 100%;\n  height: 320px;\n  border-radius: 5%;\n  margin: 0px 0px 9px; }\n\n.label_color {\n  color: #f4a942; }\n"

/***/ }),

/***/ "./src/app/detail-simplevisitor/detail-simplevisitor.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/detail-simplevisitor/detail-simplevisitor.page.ts ***!
  \*******************************************************************/
/*! exports provided: DetailSimplevisitorPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailSimplevisitorPage", function() { return DetailSimplevisitorPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var DetailSimplevisitorPage = /** @class */ (function () {
    function DetailSimplevisitorPage(apiService, webview, platform, router, zone) {
        var _this = this;
        this.apiService = apiService;
        this.webview = webview;
        this.platform = platform;
        this.router = router;
        this.zone = zone;
        this.platform.backButton.subscribe(function () {
            _this.zone.run(function () { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.router.navigate(['/listsimplevisitor'])];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); });
        });
    }
    DetailSimplevisitorPage.prototype.ngOnInit = function () {
        this.Visitordata = this.apiService.currentsimplevisitor;
    };
    DetailSimplevisitorPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-detail-simplevisitor',
            template: __webpack_require__(/*! ./detail-simplevisitor.page.html */ "./src/app/detail-simplevisitor/detail-simplevisitor.page.html"),
            styles: [__webpack_require__(/*! ./detail-simplevisitor.page.scss */ "./src/app/detail-simplevisitor/detail-simplevisitor.page.scss")],
        }),
        __metadata("design:paramtypes", [_service_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_2__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]])
    ], DetailSimplevisitorPage);
    return DetailSimplevisitorPage;
}());



/***/ })

}]);
//# sourceMappingURL=detail-simplevisitor-detail-simplevisitor-module.js.map