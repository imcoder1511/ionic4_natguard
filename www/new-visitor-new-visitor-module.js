(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["new-visitor-new-visitor-module"],{

/***/ "./src/app/new-visitor/new-visitor.module.ts":
/*!***************************************************!*\
  !*** ./src/app/new-visitor/new-visitor.module.ts ***!
  \***************************************************/
/*! exports provided: NewVisitorPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewVisitorPageModule", function() { return NewVisitorPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _new_visitor_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./new-visitor.page */ "./src/app/new-visitor/new-visitor.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _new_visitor_page__WEBPACK_IMPORTED_MODULE_5__["NewVisitorPage"]
    }
];
var NewVisitorPageModule = /** @class */ (function () {
    function NewVisitorPageModule() {
    }
    NewVisitorPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_new_visitor_page__WEBPACK_IMPORTED_MODULE_5__["NewVisitorPage"]]
        })
    ], NewVisitorPageModule);
    return NewVisitorPageModule;
}());



/***/ }),

/***/ "./src/app/new-visitor/new-visitor.page.html":
/*!***************************************************!*\
  !*** ./src/app/new-visitor/new-visitor.page.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/visitors\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      Add Visitor\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <form (ngSubmit)=\"submitVisitor()\">\n    <ion-card>\n      <ion-card-content (click)=\"takePicture()\">\n        Picture:\n        <img class=\"circle-pic\" [src]=\"base64Image\" *ngIf=\"base64Image\" />\n        <ion-icon name=\"camera\" style=\"zoom:2.0;margin-bottom: 5px;\"  float-right></ion-icon>\n        <!-- <ion-input *ngIf=\"base64Image\" type=\"hidden\" [(ngModel)]=\"visitor.image\" name=\"image\" value={{base64Image}}></ion-input> -->\n      </ion-card-content>\n    </ion-card>\n    <ion-item>\n      <ion-label position=\"floating\">Name</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"visitorModel.Name\" name=\"Name\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"floating\">Contact No</ion-label>\n      <ion-input type=\"tel\" maxlength=\"10\" [(ngModel)]=\"visitorModel.ContactNo\" name=\"ContactNo\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"floating\">Vehicle No</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"visitorModel.VehicleNo\" name=\"VehicleNo\"></ion-input>\n    </ion-item>\n\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-item>\n          <ion-label id=\"BuildingBlock\" position=\"floating\">Building Block</ion-label>\n\n          <ion-select [(ngModel)]=\"visitorModel.BuildingBlock\" name=\"BuildingBlock\" [interfaceOptions]=\"customActionSheetOptions\" interface=\"action-sheet\">\n            <ion-select-option value=\"A\">A</ion-select-option>\n            <ion-select-option value=\"B\">B</ion-select-option>\n            <ion-select-option value=\"C\">C</ion-select-option>\n            <ion-select-option value=\"D\">D</ion-select-option>\n            <ion-select-option value=\"E\">E</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-item>\n          <ion-label id=\"FlatNo\" position=\"floating\">Flat No</ion-label>\n          <ion-input type=\"number\" [(ngModel)]=\"visitorModel.FlatNo\" name=\"FlatNo\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n\n    <!-- </ion-item> -->\n    <!-- <ion-item>\n\n    </ion-item> -->\n    <ion-item>\n      <ion-label position=\"floating\">Reason</ion-label>\n      <ion-textarea type=\"text\" [(ngModel)]=\"visitorModel.Reason\" name=\"Reason\"></ion-textarea>\n    </ion-item>\n\n\n    <ion-button class=\"btn_set\" type=\"submit\" color=\"tertiary\" expand=\"block\">Submit</ion-button>\n\n  </form>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/new-visitor/new-visitor.page.scss":
/*!***************************************************!*\
  !*** ./src/app/new-visitor/new-visitor.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".circle-pic {\n  width: 100px;\n  height: 100px;\n  border-radius: 50%;\n  margin: 0 auto; }\n"

/***/ }),

/***/ "./src/app/new-visitor/new-visitor.page.ts":
/*!*************************************************!*\
  !*** ./src/app/new-visitor/new-visitor.page.ts ***!
  \*************************************************/
/*! exports provided: NewVisitorPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewVisitorPage", function() { return NewVisitorPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _util_util__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../util/util */ "./src/app/util/util.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var NewVisitorPage = /** @class */ (function () {
    function NewVisitorPage(camera, platform, router, storage, file, api_service, webview, util) {
        var _this = this;
        this.camera = camera;
        this.platform = platform;
        this.router = router;
        this.storage = storage;
        this.file = file;
        this.api_service = api_service;
        this.webview = webview;
        this.util = util;
        this.visitorModel = {
            datetime: null,
            Name: "",
            ContactNo: "",
            VehicleNo: "",
            BuildingBlock: "",
            FlatNo: "",
            Reason: "",
            Picture: "",
            Date: "",
            Time: ""
        };
        this.platform.backButton.subscribe(function () {
            _this.router.navigate(['/listsimplevisitor']);
        });
    }
    NewVisitorPage.prototype.ngOnInit = function () {
    };
    NewVisitorPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    NewVisitorPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        var newPath = "file:///storage/emulated/0/Android/data/io.ionic.visitors/Natguard_upload/";
        this.file.createDir("file:///storage/emulated/0/Android/data/io.ionic.visitors/", "Natguard_upload", false);
        this.file.copyFile(namePath, currentName, newPath, newFileName).then(function (success) {
            _this.visitorModel.Picture = newPath + newFileName;
            var date = new Date();
            _this.visitorModel.datetime = date;
            _this.visitorModel.Date = date.toLocaleDateString();
            _this.visitorModel.Time = date.toLocaleTimeString();
            _this.storage.get("visitorData").then(function (val) {
                _this.visitorData = [];
                if (JSON.parse(val) != null) {
                    _this.visitorData = JSON.parse(val);
                }
                _this.visitorData.unshift(_this.visitorModel);
                _this.storage.set("visitorData", JSON.stringify(_this.visitorData));
            });
            _this.router.navigate(['/listsimplevisitor']);
        }, function (error) {
        });
    };
    NewVisitorPage.prototype.takePicture = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.util.getFilePathFromCamera()];
                    case 1:
                        _a.img_url = _b.sent();
                        this.base64Image = this.webview.convertFileSrc(this.img_url);
                        return [2 /*return*/];
                }
            });
        });
    };
    NewVisitorPage.prototype.submitVisitor = function () {
        //* Local Code *//
        if (this.img_url == undefined) {
            this.util.toastmethod("Please add image");
        }
        else {
            var currentName = this.img_url.substr(this.img_url.lastIndexOf('/') + 1);
            var correctPath = this.img_url.substr(0, this.img_url.lastIndexOf('/') + 1);
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        }
    };
    NewVisitorPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-new-visitor',
            template: __webpack_require__(/*! ./new-visitor.page.html */ "./src/app/new-visitor/new-visitor.page.html"),
            styles: [__webpack_require__(/*! ./new-visitor.page.scss */ "./src/app/new-visitor/new-visitor.page.scss")],
        }),
        __metadata("design:paramtypes", [_ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_1__["Camera"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
            _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_6__["File"],
            _service_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_7__["WebView"],
            _util_util__WEBPACK_IMPORTED_MODULE_8__["Util"]])
    ], NewVisitorPage);
    return NewVisitorPage;
}());



/***/ })

}]);
//# sourceMappingURL=new-visitor-new-visitor-module.js.map