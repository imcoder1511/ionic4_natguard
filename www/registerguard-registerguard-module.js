(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["registerguard-registerguard-module"],{

/***/ "./src/app/registerguard/registerguard.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/registerguard/registerguard.module.ts ***!
  \*******************************************************/
/*! exports provided: RegisterguardPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterguardPageModule", function() { return RegisterguardPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _registerguard_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./registerguard.page */ "./src/app/registerguard/registerguard.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _registerguard_page__WEBPACK_IMPORTED_MODULE_5__["RegisterguardPage"]
    }
];
var RegisterguardPageModule = /** @class */ (function () {
    function RegisterguardPageModule() {
    }
    RegisterguardPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_registerguard_page__WEBPACK_IMPORTED_MODULE_5__["RegisterguardPage"]]
        })
    ], RegisterguardPageModule);
    return RegisterguardPageModule;
}());



/***/ }),

/***/ "./src/app/registerguard/registerguard.page.html":
/*!*******************************************************!*\
  !*** ./src/app/registerguard/registerguard.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/login\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Register</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <form (ngSubmit)=\"submitGuard()\">\n    <ion-card>\n      <ion-card-content (click)=\"takePicture()\">\n        Picture:\n        <img class=\"circle-pic\" [src]=\"base64Image\" *ngIf=\"base64Image\" />\n        <ion-icon name=\"camera\" style=\"zoom:2.0;margin-bottom: 5px;\"  float-right></ion-icon>\n      </ion-card-content>\n    </ion-card>\n    <ion-item>\n      <ion-label position=\"floating\">Name</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"guardModel.Name\" name=\"Name\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"floating\">Pin</ion-label>\n      <ion-input type=\"tel\" maxlength=\"4\" pattern=\"\\d{4}\" style=\"-webkit-text-security: disc;\"\n        [(ngModel)]=\"guardModel.Password\" name=\"Password\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"floating\">Society Name</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"guardModel.SocietyName\" name=\"SocietyName\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"floating\">Select Shift</ion-label>\n      <ion-select [interfaceOptions]=\"customPopoverOptions\" [(ngModel)]=\"guardModel.Shift\" name=\"Shift\" interface=\"popover\">\n        <ion-select-option value=\"Day\">Day</ion-select-option>\n        <ion-select-option value=\"Noon\">Noon</ion-select-option>\n        <ion-select-option value=\"Night\">Night</ion-select-option>\n      </ion-select>\n    </ion-item>\n    <!-- <ion-item>\n      <ion-select [interfaceOptions]=\"customPopoverOptions\" [(ngModel)]=\"guardModel.country\" name=\"country\" interface=\"popover\" (ionChange)=\"onCountryChange()\">\n          <ion-option *ngFor=\"let country of countries\" [value]=\"country\">{{country}}</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item>\n        <ion-select [interfaceOptions]=\"customPopoverOptions\" [(ngModel)]=\"guardModel.state\" name=\"state\" interface=\"popover\">\n            <ion-option *ngFor=\"let state of states\" [value]=\"state\">{{state}}</ion-option>\n        </ion-select>\n      </ion-item> -->\n    <ion-button class=\"btn_set\" type=\"submit\" color=\"tertiary\" expand=\"block\">Submit</ion-button>\n  </form>\n</ion-content>"

/***/ }),

/***/ "./src/app/registerguard/registerguard.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/registerguard/registerguard.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".circle-pic {\n  width: 100px;\n  height: 100px;\n  border-radius: 50%;\n  margin: 0 auto; }\n"

/***/ }),

/***/ "./src/app/registerguard/registerguard.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/registerguard/registerguard.page.ts ***!
  \*****************************************************/
/*! exports provided: RegisterguardPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterguardPage", function() { return RegisterguardPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _util_util__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../util/util */ "./src/app/util/util.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var RegisterguardPage = /** @class */ (function () {
    function RegisterguardPage(camera, platform, router, storage, file, api_service, webview, util, _cdr) {
        var _this = this;
        this.camera = camera;
        this.platform = platform;
        this.router = router;
        this.storage = storage;
        this.file = file;
        this.api_service = api_service;
        this.webview = webview;
        this.util = util;
        this._cdr = _cdr;
        this.guardModel = {
            guardImage: "",
            Name: "",
            Password: "",
            SocietyName: "",
            Shift: "",
        };
        this.countries = ['Afghanistan', 'Azerbaijan', 'Albania'];
        this.statesByCountry = {
            Afghanistan: ['AfghanA', 'AfghanB', 'AfghanC'],
            Azerbaijan: ['AzerA', 'AzerB', 'AzerC'],
            Albania: ['AlbaA', 'AlbaB', 'AlbaC'],
        };
        this.states = [];
        this.platform.backButton.subscribe(function () {
            _this.router.navigate(['/login']);
        });
    }
    RegisterguardPage.prototype.ngOnInit = function () {
    };
    RegisterguardPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    RegisterguardPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        var newPath = "file:///storage/emulated/0/Android/data/io.ionic.visitors/Natguard_guard/";
        this.file.createDir("file:///storage/emulated/0/Android/data/io.ionic.visitors/", "Natguard_guard", false);
        this.file.copyFile(namePath, currentName, newPath, newFileName).then(function (success) {
            _this.guardModel.guardImage = newPath + newFileName;
            _this.storage.get("guardData").then(function (val) {
                _this.guardData = [];
                if (JSON.parse(val) != null) {
                    _this.guardData = JSON.parse(val);
                }
                _this.guardData.push(_this.guardModel);
                _this.storage.set("guardData", JSON.stringify(_this.guardData));
            });
            _this.router.navigate(['/login']);
        }, function (error) {
        });
    };
    RegisterguardPage.prototype.takePicture = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.util.getFilePathFromCamera()];
                    case 1:
                        _a.img_url = _b.sent();
                        this.base64Image = this.webview.convertFileSrc(this.img_url);
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisterguardPage.prototype.onCountryChange = function () {
        // let country =  this.guardModel.country;
        // this.states = this.statesByCountry[country];
        this._cdr.detectChanges();
    };
    RegisterguardPage.prototype.submitGuard = function () {
        //* Local Code *//
        if (this.img_url == undefined) {
            this.util.toastmethod("Please add image");
        }
        else {
            var currentName = this.img_url.substr(this.img_url.lastIndexOf('/') + 1);
            var correctPath = this.img_url.substr(0, this.img_url.lastIndexOf('/') + 1);
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        }
    };
    RegisterguardPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-registerguard',
            template: __webpack_require__(/*! ./registerguard.page.html */ "./src/app/registerguard/registerguard.page.html"),
            styles: [__webpack_require__(/*! ./registerguard.page.scss */ "./src/app/registerguard/registerguard.page.scss")],
        }),
        __metadata("design:paramtypes", [_ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_1__["Camera"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"],
            _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_6__["File"],
            _service_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"],
            _util_util__WEBPACK_IMPORTED_MODULE_8__["Util"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]])
    ], RegisterguardPage);
    return RegisterguardPage;
}());



/***/ })

}]);
//# sourceMappingURL=registerguard-registerguard-module.js.map