(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./detail-permanentvisitor/detail-permanentvisitor.module": [
		"./src/app/detail-permanentvisitor/detail-permanentvisitor.module.ts",
		"detail-permanentvisitor-detail-permanentvisitor-module"
	],
	"./detail-simplevisitor/detail-simplevisitor.module": [
		"./src/app/detail-simplevisitor/detail-simplevisitor.module.ts",
		"detail-simplevisitor-detail-simplevisitor-module"
	],
	"./help/help.module": [
		"./src/app/help/help.module.ts",
		"help-help-module"
	],
	"./list-permenentvisitor/list-permenentvisitor.module": [
		"./src/app/list-permenentvisitor/list-permenentvisitor.module.ts",
		"list-permenentvisitor-list-permenentvisitor-module"
	],
	"./listsimplevisitor/listsimplevisitor.module": [
		"./src/app/listsimplevisitor/listsimplevisitor.module.ts",
		"listsimplevisitor-listsimplevisitor-module~listvisitedpermenentvisitor-listvisitedpermenentvisitor-m~dc2aa423",
		"common",
		"listsimplevisitor-listsimplevisitor-module"
	],
	"./listvisitedpermenentvisitor/listvisitedpermenentvisitor.module": [
		"./src/app/listvisitedpermenentvisitor/listvisitedpermenentvisitor.module.ts",
		"listsimplevisitor-listsimplevisitor-module~listvisitedpermenentvisitor-listvisitedpermenentvisitor-m~dc2aa423",
		"common",
		"listvisitedpermenentvisitor-listvisitedpermenentvisitor-module"
	],
	"./login/login.module": [
		"./src/app/login/login.module.ts",
		"login-login-module"
	],
	"./new-permenentvisitor/new-permenentvisitor.module": [
		"./src/app/new-permenentvisitor/new-permenentvisitor.module.ts",
		"new-permenentvisitor-new-permenentvisitor-module"
	],
	"./new-visitor/new-visitor.module": [
		"./src/app/new-visitor/new-visitor.module.ts",
		"new-visitor-new-visitor-module"
	],
	"./registerguard/registerguard.module": [
		"./src/app/registerguard/registerguard.module.ts",
		"registerguard-registerguard-module"
	],
	"./tabs/tabs.module": [
		"./src/app/tabs/tabs.module.ts",
		"tabs-tabs-module"
	],
	"./visitors/visitors.module": [
		"./src/app/visitors/visitors.module.ts",
		"visitors-visitors-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var module = __webpack_require__(ids[0]);
		return module;
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var routes = [
    // { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'newVisitor', loadChildren: './new-visitor/new-visitor.module#NewVisitorPageModule' },
    { path: 'visitors', loadChildren: './visitors/visitors.module#VisitorsPageModule' },
    { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
    { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
    { path: 'new Permenentvisitor', loadChildren: './new-permenentvisitor/new-permenentvisitor.module#NewPermenentvisitorPageModule' },
    { path: 'listPermenentvisitor', loadChildren: './list-permenentvisitor/list-permenentvisitor.module#ListPermenentvisitorPageModule' },
    { path: 'listvisitedpermenentvisitor', loadChildren: './listvisitedpermenentvisitor/listvisitedpermenentvisitor.module#ListvisitedpermenentvisitorPageModule' },
    { path: 'listsimplevisitor', loadChildren: './listsimplevisitor/listsimplevisitor.module#ListsimplevisitorPageModule' },
    { path: 'detailSimplevisitor', loadChildren: './detail-simplevisitor/detail-simplevisitor.module#DetailSimplevisitorPageModule' },
    { path: 'detailPermanentvisitor', loadChildren: './detail-permanentvisitor/detail-permanentvisitor.module#DetailPermanentvisitorPageModule' },
    { path: 'help', loadChildren: './help/help.module#HelpPageModule' },
    { path: 'registerguard', loadChildren: './registerguard/registerguard.module#RegisterguardPageModule' },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-app>\n  <ion-router-outlet></ion-router-outlet>\n</ion-app>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AppComponent = /** @class */ (function () {
    function AppComponent(platform, splashScreen, statusBar, storage, router, navCtrl) {
        var _this = this;
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.storage = storage;
        this.router = router;
        this.navCtrl = navCtrl;
        this.guard = {
            guardImage: "",
            Name: "Demo1",
            Password: "1234",
            SocietyName: "Demo Society",
            Shift: "Day"
        };
        this.defaultVisitors = [{
                BuildingBlock: "B",
                ContactNo: "9595863254",
                Date: "14/03/2019",
                FlatNo: 304,
                Name: "Mukund",
                Picture: "assets/icon/user_avt.png",
                Reason: "Relative",
                Time: "18:12:53",
                VehicleNo: "GJ 01 AJ 2022",
                datetime: "2019-03-14T12:42:53.732Z",
            }, {
                BuildingBlock: "A",
                ContactNo: "9656325415",
                Date: "14/03/2019",
                FlatNo: 402,
                Name: "Prasoon",
                Picture: "assets/icon/user_avt.png",
                Reason: "Amazon delivery man",
                Time: "18:12:53",
                VehicleNo: "MH 04 BB 2038",
                datetime: "2019-03-14T12:42:53.732Z",
            }, {
                BuildingBlock: "C",
                ContactNo: "8655423514",
                Date: "14/03/2019",
                FlatNo: 304,
                Name: "Heena",
                Picture: "assets/icon/female_avt.png",
                Reason: "Food delivery girl",
                Time: "18:12:53",
                VehicleNo: "GJ 01 BA 8568",
                datetime: "2019-03-14T12:42:53.732Z",
            }, {
                BuildingBlock: "C",
                ContactNo: "8754213652",
                Date: "14/03/2019",
                FlatNo: 301,
                Name: "Mahadeo",
                Picture: "assets/icon/user_avt.png",
                Reason: "Relative",
                Time: "18:12:53",
                VehicleNo: "GJ 01 AB 5457",
                datetime: "2019-03-14T12:42:53.732Z",
            }, {
                BuildingBlock: "B",
                ContactNo: "8866323338",
                Date: "14/03/2019",
                FlatNo: 404,
                Name: "Kailash",
                Picture: "assets/icon/user_avt.png",
                Reason: "Amazon delivery man",
                Time: "18:12:53",
                VehicleNo: "GJ 04 YU 6767",
                datetime: "2019-03-14T12:42:53.732Z",
            }, {
                BuildingBlock: "E",
                ContactNo: "8654823651",
                Date: "14/03/2019",
                FlatNo: 506,
                Name: "Dinesh",
                Picture: "assets/icon/user_avt.png",
                Reason: "Flipkart delivery man",
                Time: "18:12:53",
                VehicleNo: "GJ 05 HK 7818",
                datetime: "2019-03-14T12:42:53.732Z",
            }, {
                BuildingBlock: "A",
                ContactNo: "6425621154",
                Date: "14/03/2019",
                FlatNo: 201,
                Name: "Jobin",
                Picture: "assets/icon/user_avt.png",
                Reason: "Amazon delivery man",
                Time: "18:12:53",
                VehicleNo: "GJ 09 GH 6545",
                datetime: "2019-03-14T12:42:53.732Z",
            }, {
                BuildingBlock: "E",
                ContactNo: "8856315235",
                Date: "14/03/2019",
                FlatNo: 602,
                Name: "Nitika",
                Picture: "assets/icon/female_avt.png",
                Reason: "Relative",
                Time: "18:12:53",
                VehicleNo: "GJ 08 LK 0089",
                datetime: "2019-03-14T12:42:53.732Z",
            }, {
                BuildingBlock: "A",
                ContactNo: "9896325142",
                Date: "14/03/2019",
                FlatNo: 701,
                Name: "Gowri",
                Picture: "assets/icon/female_avt.png",
                Reason: "Food delivey girl",
                Time: "18:12:53",
                VehicleNo: "GJ 01 AA 8978",
                datetime: "2019-03-14T12:42:53.732Z",
            }, {
                BuildingBlock: "B",
                ContactNo: "6632512458",
                Date: "14/03/2019",
                FlatNo: 303,
                Name: "Riya",
                Picture: "assets/icon/female_avt.png",
                Reason: "Relative",
                Time: "18:12:53",
                VehicleNo: "GJ 05 HJ 0007",
                datetime: "2019-03-14T12:42:53.732Z",
            }, {
                BuildingBlock: "C",
                ContactNo: "8866545215",
                Date: "14/03/2019",
                FlatNo: 101,
                Name: "Bishnu",
                Picture: "assets/icon/user_avt.png",
                Reason: "Amazon delivery man",
                Time: "18:12:53",
                VehicleNo: "GJ 05 JJ 9089",
                datetime: "2019-03-14T12:42:53.732Z",
            }, {
                BuildingBlock: "E",
                ContactNo: "8460166687",
                Date: "14/03/2019",
                FlatNo: 405,
                Name: "Lalit",
                Picture: "assets/icon/user_avt.png",
                Reason: "Flipkart delivery man",
                Time: "18:12:53",
                VehicleNo: "GJ 05 AH 8978",
                datetime: "2019-03-14T12:42:53.732Z",
            }, {
                BuildingBlock: "C",
                ContactNo: "8854216354",
                Date: "14/03/2019",
                FlatNo: 502,
                Name: "Raj",
                Picture: "assets/icon/user_avt.png",
                Reason: "Relative",
                Time: "18:12:53",
                VehicleNo: "GJ 05 LH 7656",
                datetime: "2019-03-14T12:42:53.732Z",
            }];
        this.defaultPermenentvisitor = [{
                contectno: "9898253535",
                flatno: "B-604,606,702,707, A-807,101,203,405,604",
                image: "assets/icon/user_avt.png",
                purpose: "Laundry man",
                visitorname: "Shashank"
            },
            {
                contectno: "9656854521",
                flatno: "B-101,501,603,402 C-205,206,304,305,401,402",
                image: "assets/icon/female_avt.png",
                purpose: "Maid",
                visitorname: "Richa"
            }, {
                contectno: "8654326584",
                flatno: "C-505",
                image: "assets/icon/user_avt.png",
                purpose: "Newspaper man",
                visitorname: "Emran"
            }, {
                contectno: "8899656325",
                flatno: "E-404,601,604,705",
                image: "assets/icon/female_avt.png",
                purpose: "Maid",
                visitorname: "Vaishali"
            }, {
                contectno: "9254125561",
                flatno: "A-404,E-405",
                image: "assets/icon/female_avt.png",
                purpose: "Maid",
                visitorname: "Pinky"
            }, {
                contectno: "7825321453",
                flatno: "D-501,604,304 E-401,403,402",
                image: "assets/icon/user_avt.png",
                purpose: "Newspaper man",
                visitorname: "Hari"
            }, {
                contectno: "9658521453",
                flatno: "A-404,608,305,701",
                image: "assets/icon/female_avt.png",
                purpose: "Maid",
                visitorname: "Aisha"
            }, {
                contectno: "8430251012",
                flatno: "E-402,604 A-205,602",
                image: "assets/icon/user_avt.png",
                purpose: "Laundry Man",
                visitorname: "Ricky"
            }, {
                contectno: "9898983254",
                flatno: "D-202, E-204,303,402,507",
                image: "assets/icon/user_avt.png",
                purpose: "Milk man",
                visitorname: "Govind"
            }, {
                contectno: "6535552314",
                flatno: "C-401,505,302,401 D-603,705,504",
                image: "assets/icon/user_avt.png",
                purpose: "Laundry Man",
                visitorname: "Satish"
            }, {
                contectno: "6354698654",
                flatno: "A-705,D-303,305",
                image: "assets/icon/female_avt.png",
                purpose: "Maid",
                visitorname: "Nupoor"
            }];
        this.initializeApp();
        //* Local Code *//
        this.storage.get("guardData").then(function (val) {
            if (val == null || val == "" || val == undefined) {
                _this.guardData = [];
                if (JSON.parse(val) != null) {
                    _this.guardData = JSON.parse(val);
                }
                _this.guardData.push(_this.guard);
                _this.storage.set("guardData", JSON.stringify(_this.guardData));
            }
        });
        this.storage.get("visitorData").then(function (val) {
            if (val == null || val == "" || val == undefined) {
                _this.storage.set("visitorData", JSON.stringify(_this.defaultVisitors));
            }
        });
        this.storage.get("Permenentvisitor").then(function (val) {
            if (val == null || val == "" || val == undefined) {
                _this.storage.set("Permenentvisitor", JSON.stringify(_this.defaultPermenentvisitor));
            }
        });
        this.storage.get("loginUser").then(function (val) {
            if (val == null || val == "" || val == undefined) {
                _this.router.navigateByUrl('/login');
            }
            else {
                _this.router.navigateByUrl('/visitors');
            }
        });
    }
    AppComponent.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleBlackOpaque();
            _this.splashScreen.hide();
        });
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html")
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"],
            _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_2__["SplashScreen"],
            _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_3__["StatusBar"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/app-version/ngx */ "./node_modules/@ionic-native/app-version/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _moremenu_moremenu_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./moremenu/moremenu.component */ "./src/app/moremenu/moremenu.component.ts");
/* harmony import */ var _shared_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./shared.module */ "./src/app/shared.module.ts");
/* harmony import */ var _service_http__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./service/http */ "./src/app/service/http.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ionic-native/file-opener/ngx */ "./node_modules/@ionic-native/file-opener/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _list_permenentvisitor_list_permenentvisitor_page__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./list-permenentvisitor/list-permenentvisitor.page */ "./src/app/list-permenentvisitor/list-permenentvisitor.page.ts");
/* harmony import */ var _util_util__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./util/util */ "./src/app/util/util.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













// import { HttpClientModule } from '@angular/common/http';







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"], _moremenu_moremenu_component__WEBPACK_IMPORTED_MODULE_11__["MoremenuComponent"]],
            entryComponents: [_moremenu_moremenu_component__WEBPACK_IMPORTED_MODULE_11__["MoremenuComponent"]],
            imports: [
                _shared_module__WEBPACK_IMPORTED_MODULE_12__["SharedModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"].forRoot(),
                _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"],
                _ionic_storage__WEBPACK_IMPORTED_MODULE_9__["IonicStorageModule"].forRoot(),
                _angular_http__WEBPACK_IMPORTED_MODULE_14__["HttpModule"]
                // HttpClientModule
            ],
            exports: [_moremenu_moremenu_component__WEBPACK_IMPORTED_MODULE_11__["MoremenuComponent"]],
            providers: [
                _util_util__WEBPACK_IMPORTED_MODULE_19__["Util"],
                _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_10__["Camera"],
                _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_15__["File"],
                _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_16__["FileOpener"],
                _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_17__["WebView"],
                _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__["StatusBar"],
                _service_http__WEBPACK_IMPORTED_MODULE_13__["HttpProvider"],
                _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_3__["AppVersion"],
                _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__["SplashScreen"],
                { provide: _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicRouteStrategy"] },
                _list_permenentvisitor_list_permenentvisitor_page__WEBPACK_IMPORTED_MODULE_18__["ListPermenentvisitorPage"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/list-permenentvisitor/list-permenentvisitor.page.html":
/*!***********************************************************************!*\
  !*** ./src/app/list-permenentvisitor/list-permenentvisitor.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/permenentVisitor\" float-left></ion-back-button>\n    </ion-buttons>\n    <ion-title>Permanent Visitors</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button href=\"/new Permenentvisitor\" routerDirection=\"forward\">\n        <ion-icon class=\"set_icon\" name=\"person-add\" style=\"zoom:1.5;\" mode=\"ios\"></ion-icon>\n      </ion-button>\n      <ion-button href=\"/listvisitedpermenentvisitor\" routerDirection=\"forward\">\n        <ion-icon name=\"list\" style=\"zoom:1.5;\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-searchbar mode=\"ios\" (ionInput)=\"getItems($event)\" (ionClear)=\"initlizeData()\" animated></ion-searchbar>\n  <ion-list *ngIf=\"!search\">\n    <div class=\"main-visitor-box\" *ngFor=\"let item of items\">\n      <ion-card class=\"visitor-box\" (click)=\"onDetails(item)\">\n        <ion-card-content>\n          <ion-card-subtitle>\n            <div class=\"img-box\">\n              <img class=\"circle-pic\" [src]=\"this.webview.convertFileSrc(item.image)\" float-left />\n            </div>\n            <div class=\"detail-box\">\n              <p class=\"lbl_title\"> {{ item.visitorname }} </p>\n              <p class=\"lbl_subtitle\"> {{item.flatno}}</p>\n            </div>\n          </ion-card-subtitle>\n        </ion-card-content>\n      </ion-card>\n      <ion-button class=\"btnvisit\" color=\"tertiary\" size=\"small\" (click)=\"onVisit(item)\" float-right>Visit</ion-button>\n    </div>\n  </ion-list>\n\n  <ion-list *ngIf=\"search\">\n    <div class=\"main-visitor-box\" *ngFor=\"let item of filterData\">\n      <ion-card class=\"visitor-box\" (click)=\"onDetails(item)\">\n        <ion-card-content>\n          <ion-card-subtitle>\n            <div class=\"img-box\">\n              <img class=\"circle-pic\" [src]=\"this.webview.convertFileSrc(item.image)\" float-left />\n            </div>\n            <div class=\"detail-box\">\n              <p class=\"lbl_title\"> {{ item.visitorname }} </p>\n              <p class=\"lbl_subtitle\"> {{item.flatno}}</p>\n            </div>\n          </ion-card-subtitle>\n        </ion-card-content>\n      </ion-card>\n      <ion-button class=\"btnvisit\" color=\"tertiary\" size=\"small\" (click)=\"onVisit(item)\" float-right>Visit</ion-button>\n    </div>\n  </ion-list>\n  <div *ngIf=\"items.length == 0 || search && filterData.length == 0 \" text-center>\n    No Data Found\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/list-permenentvisitor/list-permenentvisitor.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/list-permenentvisitor/list-permenentvisitor.page.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".circle-pic {\n  width: 50px;\n  height: 50px;\n  border-radius: 50%;\n  margin: 0px 0px 9px; }\n\n.lbl_title {\n  padding-left: 4px !important;\n  width: 222px;\n  padding-right: 0px !important;\n  margin: 0 auto;\n  font-weight: bold;\n  font-size: 20px; }\n\np.lbl_title::first-letter {\n  text-transform: uppercase; }\n\n.lbl_subtitle {\n  padding-left: 4px !important;\n  width: 222px;\n  margin: 0 auto; }\n\n.main-visitor-box {\n  width: 100%;\n  float: left;\n  border-bottom: 1px solid #ddd;\n  min-height: 100px; }\n\n.visitor-box {\n  width: 70%;\n  float: left;\n  box-shadow: inherit;\n  background: none;\n  padding: 0; }\n\n.main-visitor-box .btnvisit {\n  float: right;\n  margin: 10px 0; }\n\n.visitor-box .card-content {\n  padding: 0px !important;\n  float: left;\n  width: 100%; }\n\n.visitor-box .card-content .hydrated {\n  margin: 0px !important;\n  float: left;\n  width: 100%; }\n\n.visitor-box .card-content .hydrated img {\n  margin: 0px 10px 9px 0; }\n\n.visitor-box .img-box {\n  float: left;\n  width: 65px; }\n\n.visitor-box .detail-box {\n  float: left;\n  width: 73%; }\n\n.visitor-box .detail-box p {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/list-permenentvisitor/list-permenentvisitor.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/list-permenentvisitor/list-permenentvisitor.page.ts ***!
  \*********************************************************************/
/*! exports provided: ListPermenentvisitorPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListPermenentvisitorPage", function() { return ListPermenentvisitorPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var _util_util__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../util/util */ "./src/app/util/util.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ListPermenentvisitorPage = /** @class */ (function () {
    function ListPermenentvisitorPage(platform, router, util, storage, webview, apiService) {
        var _this = this;
        this.platform = platform;
        this.router = router;
        this.util = util;
        this.storage = storage;
        this.webview = webview;
        this.apiService = apiService;
        this.items = [];
        this.filterData = [];
        this.search = false;
        this.initlizeData();
        this.platform.backButton.subscribe(function () {
            _this.router.navigate(['/visitors']);
        });
    }
    ListPermenentvisitorPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.platform.backButton.subscribe(function () {
            _this.router.navigateByUrl('/visitors');
        });
    };
    ListPermenentvisitorPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.initlizeData();
        this.platform.backButton.subscribe(function () {
            _this.router.navigateByUrl('/visitors');
        });
    };
    ListPermenentvisitorPage.prototype.ngOnInit = function () {
    };
    ListPermenentvisitorPage.prototype.initlizeData = function () {
        var _this = this;
        this.storage.get("Permenentvisitor").then(function (val) {
            if (JSON.parse(val) != null) {
                _this.search = false;
                _this.items = JSON.parse(val);
            }
        });
    };
    ListPermenentvisitorPage.prototype.getItems = function (ev) {
        this.search = true;
        var val = ev.detail.data;
        if (val && val.trim() != '') {
            this.filterData = this.items.filter(function (item) {
                return item.visitorname.toLowerCase().indexOf(val.toLowerCase()) > -1;
            });
        }
        else {
            this.initlizeData();
        }
    };
    ListPermenentvisitorPage.prototype.onVisit = function (item) {
        var _this = this;
        var date = new Date();
        item.Date = date.toLocaleDateString();
        item.Time = date.toLocaleTimeString();
        item.datetime = date;
        this.storage.get("Visited").then(function (val) {
            _this.visited = [];
            if (JSON.parse(val) != null) {
                _this.visited = JSON.parse(val);
            }
            _this.visited.unshift(item);
            _this.storage.set("Visited", JSON.stringify(_this.visited));
            _this.util.toastmethod('Visitor have been visited successfully.');
        });
    };
    ListPermenentvisitorPage.prototype.onDetails = function (item) {
        this.apiService.currentpermanentvisitor = item;
        this.router.navigate(['/detailPermanentvisitor', { buttonFlag: true }]);
    };
    ListPermenentvisitorPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-permenentvisitor',
            template: __webpack_require__(/*! ./list-permenentvisitor.page.html */ "./src/app/list-permenentvisitor/list-permenentvisitor.page.html"),
            styles: [__webpack_require__(/*! ./list-permenentvisitor.page.scss */ "./src/app/list-permenentvisitor/list-permenentvisitor.page.scss")],
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _util_util__WEBPACK_IMPORTED_MODULE_6__["Util"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_4__["WebView"],
            _service_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"]])
    ], ListPermenentvisitorPage);
    return ListPermenentvisitorPage;
}());



/***/ }),

/***/ "./src/app/moremenu/moremenu.component.html":
/*!**************************************************!*\
  !*** ./src/app/moremenu/moremenu.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-list lines=\"full\" style=\"margin:0\">\n    <ion-item (click)=\"permenentVisitor()\">\n        <ion-icon class=\"set_icon\" name=\"person-add\" mode=\"ios\"></ion-icon>\n        <ion-label>&nbsp;&nbsp;&nbsp;Permenent Visitor</ion-label>\n    </ion-item>\n    <ion-item (click)=\"help()\">\n        <ion-icon name=\"information-circle\"></ion-icon>\n       <ion-label>&nbsp;&nbsp;&nbsp;Help</ion-label>\n    </ion-item>\n    <ion-item (click)=\"offship()\">\n        <ion-icon name=\"log-out\"></ion-icon>\n       <ion-label> &nbsp;&nbsp;&nbsp;Off-ship</ion-label>\n    </ion-item>\n    <ion-item>\n        <ion-label float-right>Version : {{version}}</ion-label>\n    </ion-item>\n</ion-list>"

/***/ }),

/***/ "./src/app/moremenu/moremenu.component.scss":
/*!**************************************************!*\
  !*** ./src/app/moremenu/moremenu.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/moremenu/moremenu.component.ts":
/*!************************************************!*\
  !*** ./src/app/moremenu/moremenu.component.ts ***!
  \************************************************/
/*! exports provided: MoremenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MoremenuComponent", function() { return MoremenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/app-version/ngx */ "./node_modules/@ionic-native/app-version/ngx/index.js");
/* harmony import */ var _util_util__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../util/util */ "./src/app/util/util.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MoremenuComponent = /** @class */ (function () {
    function MoremenuComponent(router, storage, util, popoverController, appVersion) {
        var _this = this;
        this.router = router;
        this.storage = storage;
        this.util = util;
        this.popoverController = popoverController;
        this.appVersion = appVersion;
        this.Logindata = [
            {
                UserName: "Ramesh",
                Password: "123456",
                Shift: "Day"
            },
            {
                UserName: "Mahesh",
                Password: "123456",
                Shift: "Noon"
            },
            {
                UserName: "Hiren",
                Password: "123456",
                Shift: "Night"
            }
        ];
        this.appVersion.getVersionNumber().then(function (data) {
            _this.version = data;
        }, function (error) {
        });
    }
    MoremenuComponent.prototype.permenentVisitor = function () {
        this.util.dismissPopover();
        this.router.navigate(["/listPermenentvisitor"]);
    };
    MoremenuComponent.prototype.offship = function () {
        this.storage.set("loginUser", "");
        this.util.dismissPopover();
        this.router.navigate(["/login"]);
    };
    MoremenuComponent.prototype.help = function () {
        this.util.dismissPopover();
        this.router.navigate(["/help"]);
    };
    MoremenuComponent.prototype.ngOnInit = function () {
    };
    MoremenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-moremenu',
            template: __webpack_require__(/*! ./moremenu.component.html */ "./src/app/moremenu/moremenu.component.html"),
            styles: [__webpack_require__(/*! ./moremenu.component.scss */ "./src/app/moremenu/moremenu.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_1__["Storage"],
            _util_util__WEBPACK_IMPORTED_MODULE_5__["Util"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"],
            _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_4__["AppVersion"]])
    ], MoremenuComponent);
    return MoremenuComponent;
}());



/***/ }),

/***/ "./src/app/service/api.service.ts":
/*!****************************************!*\
  !*** ./src/app/service/api.service.ts ***!
  \****************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./http */ "./src/app/service/http.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { HttpClient } from '@angular/common/http';
// import { HTTP } from '@ionic-native/http';

var API_URL = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].api_url;
var ApiService = /** @class */ (function () {
    function ApiService(httpclient) {
        this.httpclient = httpclient;
    }
    ApiService.prototype.testApi = function (data) {
        this.httpclient.post("https://jsonplaceholder.typicode.com/posts", data).subscribe(function (data) {
        }, function (err) {
        });
    };
    ApiService.prototype.syncVisitor = function (syncData) {
        //  this.httpclient.addHeader("Content-Type", "application/json");
        return this.httpclient.post(API_URL + "Visitor/PostTempVisitor", syncData).subscribe(function (data) {
        }, function (err) {
        });
    };
    ApiService.prototype.addVisitor = function (Visitordata) {
        return this.httpclient.post(API_URL + "Visitor/CreateVisitor", Visitordata);
    };
    ApiService.prototype.listVisitor = function () {
        return this.httpclient.get("http://192.168.0.125:1050/api/Project/GetAllClients");
    };
    // addPermenentvisitor(data){
    //   this.httpclient.post(API_URL+"",data);
    // }
    // listPermenentvisitor(){
    //   return this.httpclient.get(API_URL+"");
    // }
    // visitedPermenentvisitor(data){
    //   this.httpclient.post(API_URL+"",data);
    // }
    ApiService.prototype.login = function (userdata) {
        return this.httpclient.post(API_URL + "Account/Login", userdata);
    };
    ApiService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_http__WEBPACK_IMPORTED_MODULE_2__["HttpProvider"]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "./src/app/service/http.ts":
/*!*********************************!*\
  !*** ./src/app/service/http.ts ***!
  \*********************************/
/*! exports provided: HttpProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpProvider", function() { return HttpProvider; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/operator/catch */ "./node_modules/rxjs-compat/_esm5/add/operator/catch.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import 'rxjs/add/operator/map';




var HttpProvider = /** @class */ (function () {
    function HttpProvider(http) {
        this.http = http;
    }
    //   addHeader(headerName: string, headerValue: string) {
    //     if (!this.headers) {
    //       this.headers = new Headers();
    //     }
    //     (this.headers).set(headerName, headerValue);
    //   }
    HttpProvider.prototype.get = function (url) {
        return this.http.get(url, { headers: this.headers }).map(function (res) { return res.json(); }).catch(this.handleError).do(function (data) {
        }, function (error) {
        });
    };
    HttpProvider.prototype.delete = function (url) {
        return this.http.delete(url, { headers: this.headers }).map(function (res) { return res.json(); }).catch(this.handleError).do(function (data) {
        }, function (error) {
        });
    };
    HttpProvider.prototype.post = function (url, data) {
        return this.http.post(url, data, { headers: this.headers }).map(function (res) { return res.json(); }).catch(this.handleError).do(function (data) {
        }, function (error) {
        });
    };
    HttpProvider.prototype.handleError = function (error) {
        return rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"].throw(error.json().error || 'Server error');
    };
    HttpProvider = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], HttpProvider);
    return HttpProvider;
}());



/***/ }),

/***/ "./src/app/shared.module.ts":
/*!**********************************!*\
  !*** ./src/app/shared.module.ts ***!
  \**********************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var time_ago_pipe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! time-ago-pipe */ "./node_modules/time-ago-pipe/esm5/time-ago-pipe.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [],
            declarations: [
                time_ago_pipe__WEBPACK_IMPORTED_MODULE_1__["TimeAgoPipe"]
            ],
            exports: [
                time_ago_pipe__WEBPACK_IMPORTED_MODULE_1__["TimeAgoPipe"]
            ]
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/app/util/util.ts":
/*!******************************!*\
  !*** ./src/app/util/util.ts ***!
  \******************************/
/*! exports provided: Util */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Util", function() { return Util; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var Util = /** @class */ (function () {
    function Util(alertController, toastController, loadingController, popoverController, camera) {
        this.alertController = alertController;
        this.toastController = toastController;
        this.loadingController = loadingController;
        this.popoverController = popoverController;
        this.camera = camera;
        this.isLoading = false;
        // declare cameraOptions
        this.cameraOptions = {
            quality: 100,
            sourceType: this.camera.PictureSourceType.CAMERA,
            destinationType: this.camera.DestinationType.FILE_URI,
            correctOrientation: true,
            encodingType: this.camera.EncodingType.JPEG,
            cameraDirection: this.camera.Direction.FRONT,
        };
    }
    // Active toast message
    Util.prototype.toastmethod = function (message) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: message,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    // active loading
    Util.prototype.presentLoading = function (message) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = true;
                        return [4 /*yield*/, this.loadingController.create({
                                duration: 5000,
                            }).then(function (a) {
                                a.present().then(function () {
                                    console.log('presented');
                                    if (!_this.isLoading) {
                                        a.dismiss().then(function () { return console.log('abort presenting'); });
                                    }
                                });
                            })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    //  dismiss loading
    Util.prototype.dismissLoading = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = false;
                        return [4 /*yield*/, this.loadingController.dismiss().then(function () { return console.log('dismissed'); })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    // active popover
    Util.prototype.presentPopover = function (component) {
        return __awaiter(this, void 0, void 0, function () {
            var popover;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.popoverController.create({
                            component: component,
                            event: event,
                            translucent: true
                        })];
                    case 1:
                        popover = _a.sent();
                        return [4 /*yield*/, popover.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    // dismiss popover
    Util.prototype.dismissPopover = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.popoverController.dismiss()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    //Alert box
    Util.prototype.presentAlert = function (header, message, success) {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: header,
                            message: message,
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                        //       console.log('Confirm Cancel: blah');
                                    }
                                }, {
                                    text: 'Okay',
                                    handler: function () {
                                        //       console.log('Confirm Okay');
                                        success();
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // FilePathFromCamera
    Util.prototype.getFilePathFromCamera = function () {
        return __awaiter(this, void 0, void 0, function () {
            var imagePath;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.camera.getPicture(this.cameraOptions)];
                    case 1:
                        imagePath = _a.sent();
                        return [2 /*return*/, imagePath];
                }
            });
        });
    };
    Util = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["PopoverController"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_2__["Camera"]])
    ], Util);
    return Util;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    api_url: "http://localhost:1122/api/"
    // http://192.168.0.111:1122/api/Visitor/PostTempVisitor
    //  http://192.168.0.111:1010/api/
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/ns-mac/Harsh/LiveApp/NatguardApp/ionic4_natguard/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map