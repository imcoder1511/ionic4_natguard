(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["visitors-visitors-module"],{

/***/ "./src/app/visitors/visitors.module.ts":
/*!*********************************************!*\
  !*** ./src/app/visitors/visitors.module.ts ***!
  \*********************************************/
/*! exports provided: VisitorsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitorsPageModule", function() { return VisitorsPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _visitors_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./visitors.page */ "./src/app/visitors/visitors.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _visitors_page__WEBPACK_IMPORTED_MODULE_5__["VisitorsPage"]
    }
];
var VisitorsPageModule = /** @class */ (function () {
    function VisitorsPageModule() {
    }
    VisitorsPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_visitors_page__WEBPACK_IMPORTED_MODULE_5__["VisitorsPage"]]
        })
    ], VisitorsPageModule);
    return VisitorsPageModule;
}());



/***/ }),

/***/ "./src/app/visitors/visitors.page.html":
/*!*********************************************!*\
  !*** ./src/app/visitors/visitors.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title text-center>NatGuard\n    </ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"presentPopover($event)\">\n        <ion-icon name=\"more\" float-right></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding class=\"visitor-name-detail\">\n  <ion-list>\n    <ion-card>\n      <ion-card-content>\n        <img class=\"circle-pic\" [src]=\"guardImage\" *ngIf=\"guardImage\" />\n        <ion-label class=\"set_label_style\">\n          <b>\n            Guard Name\n          </b>\n        </ion-label>\n        <ion-label class=\"set_label_style label_color\">\n          <span>\n            {{guardName}}\n          </span>\n        </ion-label>\n\n\n        <ion-label class=\"set_label_style\">\n          <b>\n            Society Name\n          </b>\n        </ion-label>\n        <ion-label class=\"set_label_style label_color\">\n          <span>\n            {{societyName}}\n          </span>\n        </ion-label>\n\n        <!-- <ion-label class=\"set_label_style\" style=\"margin-top: 10%\">Society Name: <span\n            class=\"set_label_style label_color\">&nbsp;\n            &nbsp;{{societyName}}</span></ion-label> -->\n\n        <ion-grid class=\"visitor-display-box\">\n          <ion-row align-items-center>\n            <ion-col>\n              <ion-card (click)=\"pushVisitor()\">\n                <ion-list class=\"card_header_color\">\n                  <ion-item>\n                    <ion-icon class=\"icon_style\" name=\"contact\" mode=\"ios\"></ion-icon>\n                  </ion-item>\n                  <ion-item>\n                    <ion-label class=\"label_style\">Visitor</ion-label>\n                  </ion-item>\n                </ion-list>\n              </ion-card>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-card-content>\n    </ion-card>\n  </ion-list>\n</ion-content>"

/***/ }),

/***/ "./src/app/visitors/visitors.page.scss":
/*!*********************************************!*\
  !*** ./src/app/visitors/visitors.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".label_color {\n  color: #f4a942; }\n\n.circle-pic {\n  width: 100px;\n  height: 100px;\n  border-radius: 50%;\n  margin: 10% auto; }\n\n.visitor-name-detail {\n  height: 100%;\n  right: 0;\n  margin: 0; }\n\n.visitor-name-detail ion-list {\n  height: 100%;\n  border: 1px solid #ddd; }\n\n.visitor-name-detail ion-list .card-content {\n  height: 100%; }\n\n.visitor-name-detail ion-card {\n  height: 100%;\n  margin: 0; }\n\n.visitor-name-detail ion-card .set_label_style {\n  text-align: center; }\n\n.visitor-name-detail ion-card .set_label_style b, .visitor-name-detail ion-card .set_label_style span {\n  width: 100%;\n  top: 10%;\n  font-size: 16px; }\n\n.visitor-display-box {\n  position: absolute;\n  bottom: 0;\n  width: 100%;\n  left: 0;\n  top: 75%;\n  -webkit-transform: translateY(-60%);\n          transform: translateY(-60%);\n  right: 0; }\n"

/***/ }),

/***/ "./src/app/visitors/visitors.page.ts":
/*!*******************************************!*\
  !*** ./src/app/visitors/visitors.page.ts ***!
  \*******************************************/
/*! exports provided: VisitorsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitorsPage", function() { return VisitorsPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _moremenu_moremenu_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../moremenu/moremenu.component */ "./src/app/moremenu/moremenu.component.ts");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _util_util__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../util/util */ "./src/app/util/util.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var VisitorsPage = /** @class */ (function () {
    function VisitorsPage(util, router, platform, storage, popoverController, webview) {
        this.util = util;
        this.router = router;
        this.platform = platform;
        this.storage = storage;
        this.popoverController = popoverController;
        this.webview = webview;
        this.util.dismissLoading();
    }
    VisitorsPage.prototype.ionViewDidEnter = function () {
        this.subscription = this.platform.backButton.subscribe(function () {
            navigator['app'].exitApp();
        });
    };
    VisitorsPage.prototype.ionViewWillLeave = function () {
        this.subscription.unsubscribe();
    };
    VisitorsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get('loginUser').then(function (val) {
            _this.guardName = val;
        });
        this.storage.get('societyName').then(function (val) {
            _this.societyName = val;
        });
        this.storage.get('guardImage').then(function (val) {
            if (val != null || val != "" || val != undefined) {
                _this.guardImage = _this.webview.convertFileSrc(val);
            }
            else {
                _this.guardImage = "assets/icon/if_profle_1055000.png";
            }
        });
    };
    VisitorsPage.prototype.ngOnInit = function () {
    };
    VisitorsPage.prototype.pushVisitor = function () {
        this.router.navigate(['/listsimplevisitor']);
    };
    VisitorsPage.prototype.clearls = function () {
        this.storage.clear();
    };
    VisitorsPage.prototype.presentPopover = function (ev) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.util.presentPopover(_moremenu_moremenu_component__WEBPACK_IMPORTED_MODULE_4__["MoremenuComponent"]);
                return [2 /*return*/];
            });
        });
    };
    VisitorsPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-visitors',
            template: __webpack_require__(/*! ./visitors.page.html */ "./src/app/visitors/visitors.page.html"),
            styles: [__webpack_require__(/*! ./visitors.page.scss */ "./src/app/visitors/visitors.page.scss")],
        }),
        __metadata("design:paramtypes", [_util_util__WEBPACK_IMPORTED_MODULE_6__["Util"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__["WebView"]])
    ], VisitorsPage);
    return VisitorsPage;
}());



/***/ })

}]);
//# sourceMappingURL=visitors-visitors-module.js.map