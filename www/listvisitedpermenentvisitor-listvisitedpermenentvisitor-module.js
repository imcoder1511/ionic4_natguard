(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["listvisitedpermenentvisitor-listvisitedpermenentvisitor-module"],{

/***/ "./src/app/listvisitedpermenentvisitor/listvisitedpermenentvisitor.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/listvisitedpermenentvisitor/listvisitedpermenentvisitor.module.ts ***!
  \***********************************************************************************/
/*! exports provided: ListvisitedpermenentvisitorPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListvisitedpermenentvisitorPageModule", function() { return ListvisitedpermenentvisitorPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _listvisitedpermenentvisitor_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./listvisitedpermenentvisitor.page */ "./src/app/listvisitedpermenentvisitor/listvisitedpermenentvisitor.page.ts");
/* harmony import */ var _shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared.module */ "./src/app/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import {TimeAgoPipe} from 'time-ago-pipe';



var routes = [
    {
        path: '',
        component: _listvisitedpermenentvisitor_page__WEBPACK_IMPORTED_MODULE_5__["ListvisitedpermenentvisitorPage"]
    }
];
var ListvisitedpermenentvisitorPageModule = /** @class */ (function () {
    function ListvisitedpermenentvisitorPageModule() {
    }
    ListvisitedpermenentvisitorPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_listvisitedpermenentvisitor_page__WEBPACK_IMPORTED_MODULE_5__["ListvisitedpermenentvisitorPage"]],
        })
    ], ListvisitedpermenentvisitorPageModule);
    return ListvisitedpermenentvisitorPageModule;
}());



/***/ }),

/***/ "./src/app/listvisitedpermenentvisitor/listvisitedpermenentvisitor.page.html":
/*!***********************************************************************************!*\
  !*** ./src/app/listvisitedpermenentvisitor/listvisitedpermenentvisitor.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/permenentVisitor\" float-left></ion-back-button>\n    </ion-buttons>\n    <ion-title>Visited Visitors</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"savePDF()\">\n        <ion-icon name=\"save\" style=\"zoom:1.5;\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-list>\n    <ion-card *ngFor=\"let item of visitedList\" (click)=\"permenentvisitorDetail(item)\">\n      <ion-card-content>\n        <ion-card-subtitle>\n          <img class=\"circle-pic\" [src]=\"this.webview.convertFileSrc(item.image)\" float-left />\n          <p class=\"lbl_title\"> {{ item.visitorname }} </p>\n          <p class=\"lbl_subtitle\"> Flat No: {{item.flatno}}</p>\n          <p class=\"lbl_subtitle\"> {{item.datetime |timeAgo}}</p>\n\n        </ion-card-subtitle>\n      </ion-card-content>\n    </ion-card>\n  </ion-list>\n\n  <div *ngIf=\"visitedList.length == 0\" text-center>\n      No Data Found\n  </div>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/listvisitedpermenentvisitor/listvisitedpermenentvisitor.page.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/listvisitedpermenentvisitor/listvisitedpermenentvisitor.page.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".circle-pic {\n  width: 50px;\n  height: 50px;\n  border-radius: 50%;\n  margin: 0px 0px 9px; }\n\n.btnvisit {\n  position: absolute;\n  right: 0;\n  top: 5px; }\n\n.lbl_title {\n  padding-left: 4px !important;\n  width: 222px;\n  padding-right: 0px !important;\n  margin: 0 auto;\n  font-weight: bold;\n  font-size: 20px; }\n\np.lbl_title::first-letter {\n  text-transform: uppercase; }\n\n.lbl_subtitle {\n  padding-left: 4px !important;\n  width: 222px;\n  margin: 0 auto; }\n"

/***/ }),

/***/ "./src/app/listvisitedpermenentvisitor/listvisitedpermenentvisitor.page.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/listvisitedpermenentvisitor/listvisitedpermenentvisitor.page.ts ***!
  \*********************************************************************************/
/*! exports provided: ListvisitedpermenentvisitorPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListvisitedpermenentvisitorPage", function() { return ListvisitedpermenentvisitorPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jspdf */ "./node_modules/jspdf/dist/jspdf.min.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jspdf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file-opener/ngx */ "./node_modules/@ionic-native/file-opener/ngx/index.js");
/* harmony import */ var _util_util__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../util/util */ "./src/app/util/util.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





__webpack_require__(/*! jspdf-autotable */ "./node_modules/jspdf-autotable/dist/jspdf.plugin.autotable.js");





var ListvisitedpermenentvisitorPage = /** @class */ (function () {
    function ListvisitedpermenentvisitorPage(platform, router, storage, file, fileOpener, webview, alertController, apiService, util) {
        var _this = this;
        this.platform = platform;
        this.router = router;
        this.storage = storage;
        this.file = file;
        this.fileOpener = fileOpener;
        this.webview = webview;
        this.alertController = alertController;
        this.apiService = apiService;
        this.util = util;
        this.visitedList = [];
        this.initlizeData();
        this.platform.backButton.subscribe(function () {
            _this.router.navigate(['/listPermenentvisitor']);
        });
    }
    ListvisitedpermenentvisitorPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.platform.backButton.subscribe(function () {
            _this.router.navigateByUrl('/listPermenentvisitor');
        });
    };
    ListvisitedpermenentvisitorPage.prototype.getLocalData = function () {
        var _this = this;
        this.storage.get("Visited").then(function (val) {
            if (JSON.parse(val) != null) {
                _this.visitedList = JSON.parse(val);
            }
        });
    };
    ListvisitedpermenentvisitorPage.prototype.initlizeData = function () {
        var directory = "file:///storage/emulated/0/Download/";
        var fileName = "PermenentVisitors.pdf";
        this.file.removeFile(directory, fileName);
        this.getLocalData();
    };
    ListvisitedpermenentvisitorPage.prototype.createPdf = function (that) {
        that.initlizeData();
        that.getLocalData();
        var gname;
        that.storage.get('loginUser').then(function (val) {
            gname = val;
            var columns = [
                { title: "Name", dataKey: "visitorname" },
                { title: "FlatNo", dataKey: "flatno" },
                { title: "ContectNO", dataKey: "contectno" },
                { title: "Purpose", dataKey: "purpose" },
                { title: "Date", dataKey: "Date" },
                { title: "Time", dataKey: "Time" },
            ];
            var images = [];
            var i = 0;
            var rows = that.visitedList;
            var doc = new jspdf__WEBPACK_IMPORTED_MODULE_4__('p', 'pt');
            doc.autoTable(columns, rows, {
                theme: 'striped',
                headerStyles: { fillColor: [73, 29, 0] },
                columnStyles: {
                    visitorname: { columnWidth: 80 },
                    flatno: { columnWidth: 80 },
                    purpose: { columnWidth: 80 }
                },
                margin: { top: 80 },
                addPageContent: function (data) {
                    doc.setFontSize(30);
                    doc.text("NatGuard", 40, 40);
                    doc.setFontSize(15);
                    doc.text("Guard Name:" + gname, 40, 70);
                }
            });
            doc.save('PermenentVisitors.pdf');
            var pdfOutput = doc.output();
            var buffer = new ArrayBuffer(pdfOutput.length);
            var array = new Uint8Array(buffer);
            for (var i = 0; i < pdfOutput.length; i++) {
                array[i] = pdfOutput.charCodeAt(i);
            }
            // For that, you have to use ionic native file plugin
            var directory = "file:///storage/emulated/0/Download/";
            var fileName = "PermenentVisitors.pdf";
            that.file.writeFile(directory, fileName, buffer)
                .then(function (success) {
                that.fileOpener.open(directory + fileName, 'application/pdf');
            })
                .catch(function (error) {
            });
            that.util.toastmethod('Report have been saved successfully.');
        });
    };
    ListvisitedpermenentvisitorPage.prototype.savePDF = function () {
        return __awaiter(this, void 0, void 0, function () {
            var that;
            var _this = this;
            return __generator(this, function (_a) {
                that = this;
                this.util.presentAlert('Save File!', '<strong>Your file stored in download folder</strong> <br/>Are you want to save your file?', function () { _this.createPdf(that); });
                return [2 /*return*/];
            });
        });
    };
    ListvisitedpermenentvisitorPage.prototype.ngOnInit = function () {
    };
    ListvisitedpermenentvisitorPage.prototype.permenentvisitorDetail = function (item) {
        this.apiService.currentpermanentvisitor = item;
        this.router.navigateByUrl('/detailPermanentvisitor');
    };
    ListvisitedpermenentvisitorPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-listvisitedpermenentvisitor',
            template: __webpack_require__(/*! ./listvisitedpermenentvisitor.page.html */ "./src/app/listvisitedpermenentvisitor/listvisitedpermenentvisitor.page.html"),
            styles: [__webpack_require__(/*! ./listvisitedpermenentvisitor.page.scss */ "./src/app/listvisitedpermenentvisitor/listvisitedpermenentvisitor.page.scss")],
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"],
            _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_5__["File"],
            _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__["FileOpener"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"],
            _service_api_service__WEBPACK_IMPORTED_MODULE_7__["ApiService"],
            _util_util__WEBPACK_IMPORTED_MODULE_9__["Util"]])
    ], ListvisitedpermenentvisitorPage);
    return ListvisitedpermenentvisitorPage;
}());



/***/ })

}]);
//# sourceMappingURL=listvisitedpermenentvisitor-listvisitedpermenentvisitor-module.js.map