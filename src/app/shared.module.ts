import { NgModule, Directive,OnInit, EventEmitter, Output, OnDestroy, Input,ElementRef,Renderer } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TimeAgoPipe} from 'time-ago-pipe';

@NgModule({
  imports: [
  ],
  declarations: [
    TimeAgoPipe
  ],
  exports: [
    TimeAgoPipe
  ]
})

export class SharedModule { }