import { Component, OnInit, NgZone } from '@angular/core';
import { ApiService } from '../service/api.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-detail-simplevisitor',
  templateUrl: './detail-simplevisitor.page.html',
  styleUrls: ['./detail-simplevisitor.page.scss'],
})
export class DetailSimplevisitorPage implements OnInit {

  public Visitordata: any;
  constructor(public apiService: ApiService,
              public webview: WebView,
              private platform: Platform,
              private router: Router,
              private zone: NgZone) {
                this.platform.backButton.subscribe(() => {
                  this.zone.run(async () => {
                  await this.router.navigate(['/listsimplevisitor']);
                  });
                });
            
               }

  ngOnInit() {
    this.Visitordata = this.apiService.currentsimplevisitor;
  }

}
