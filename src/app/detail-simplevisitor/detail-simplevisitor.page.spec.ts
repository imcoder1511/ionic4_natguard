import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailSimplevisitorPage } from './detail-simplevisitor.page';

describe('DetailSimplevisitorPage', () => {
  let component: DetailSimplevisitorPage;
  let fixture: ComponentFixture<DetailSimplevisitorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailSimplevisitorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailSimplevisitorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
