import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetailSimplevisitorPage } from './detail-simplevisitor.page';

const routes: Routes = [
  {
    path: '',
    component: DetailSimplevisitorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetailSimplevisitorPage]
})
export class DetailSimplevisitorPageModule {}
