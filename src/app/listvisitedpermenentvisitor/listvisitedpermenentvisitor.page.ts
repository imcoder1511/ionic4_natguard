import { Component, OnInit } from '@angular/core';
import { Platform, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import * as jsPDF from 'jspdf'
require('jspdf-autotable');
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ApiService } from '../service/api.service';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Util } from '../util/util';


@Component({
  selector: 'app-listvisitedpermenentvisitor',
  templateUrl: './listvisitedpermenentvisitor.page.html',
  styleUrls: ['./listvisitedpermenentvisitor.page.scss'],
})
export class ListvisitedpermenentvisitorPage implements OnInit {
  public visitedList: any = [];
  public that;
  constructor(public platform: Platform,
    public router: Router,
    public storage: Storage,
    public file: File,
    public fileOpener: FileOpener,
    public webview: WebView,
    public alertController: AlertController,
    public apiService: ApiService,
    public util: Util) {
    this.initlizeData();
    this.platform.backButton.subscribe(() => {
      this.router.navigate(['/listPermenentvisitor']);
    });
  }

  ionViewWillEnter() {
    this.platform.backButton.subscribe(() => {
      this.router.navigateByUrl('/listPermenentvisitor');
    });
  }

  public getLocalData() {
    this.storage.get("Visited").then(val => {
      if (JSON.parse(val) != null) {
        this.visitedList = JSON.parse(val);
      }
    });
  }

  public initlizeData() {
    const directory = "file:///storage/emulated/0/Download/";

    const fileName = "PermenentVisitors.pdf";
    this.file.removeFile(directory, fileName)
    this.getLocalData();
  }

  public createPdf(that: any) {

    that.initlizeData();
    that.getLocalData();
    var gname;
    that.storage.get('loginUser').then((val) => {
      gname = val
      var columns = [
        { title: "Name", dataKey: "visitorname" },
        { title: "FlatNo", dataKey: "flatno" },
        { title: "ContectNO", dataKey: "contectno" },
        { title: "Purpose", dataKey: "purpose" },
        { title: "Date", dataKey: "Date" },
        { title: "Time", dataKey: "Time" },
      ];
      var images = [];
      var i = 0;
      var rows = that.visitedList;
      var doc = new jsPDF('p', 'pt');
      doc.autoTable(columns, rows, {
        theme: 'striped',
        headerStyles: { fillColor: [73, 29, 0] },
        columnStyles: {
          visitorname: { columnWidth: 80 },
          flatno: { columnWidth: 80 },
          purpose: { columnWidth: 80 }
        },
        margin: { top: 80 },
        addPageContent: function (data) {
          doc.setFontSize(30);
          doc.text("NatGuard", 40, 40);
          doc.setFontSize(15);
          doc.text("Guard Name:" + gname, 40, 70);
        }
      });
      doc.save('PermenentVisitors.pdf');
      let pdfOutput = doc.output();

      let buffer = new ArrayBuffer(pdfOutput.length);

      let array = new Uint8Array(buffer);

      for (var i = 0; i < pdfOutput.length; i++) {
        array[i] = pdfOutput.charCodeAt(i);
      }

      // For that, you have to use ionic native file plugin
      const directory = "file:///storage/emulated/0/Download/";

      const fileName = "PermenentVisitors.pdf";
      that.file.writeFile(directory, fileName, buffer)
        .then((success) => {
          that.fileOpener.open(directory + fileName, 'application/pdf');
        })
        .catch((error) => {
        });
      that.util.toastmethod('Report have been saved successfully.');
    });
  }

  public async savePDF() {
    var that = this;
    this.util.presentAlert('Save File!', '<strong>Your file stored in download folder</strong> <br/>Are you want to save your file?', () => { this.createPdf(that) })
  }

  ngOnInit() {
  }

  public permenentvisitorDetail(item) {
    this.apiService.currentpermanentvisitor = item
    this.router.navigateByUrl('/detailPermanentvisitor');
  }

}
