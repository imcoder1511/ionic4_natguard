import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListvisitedpermenentvisitorPage } from './listvisitedpermenentvisitor.page';

describe('ListvisitedpermenentvisitorPage', () => {
  let component: ListvisitedpermenentvisitorPage;
  let fixture: ComponentFixture<ListvisitedpermenentvisitorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListvisitedpermenentvisitorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListvisitedpermenentvisitorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
