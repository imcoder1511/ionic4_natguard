import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
// import {TimeAgoPipe} from 'time-ago-pipe';


import { IonicModule } from '@ionic/angular';

import { ListvisitedpermenentvisitorPage } from './listvisitedpermenentvisitor.page';
import { SharedModule } from '../shared.module';

const routes: Routes = [
  {
    path: '',
    component: ListvisitedpermenentvisitorPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListvisitedpermenentvisitorPage],
  // exports:[TimeAgoPipe]
})
export class ListvisitedpermenentvisitorPageModule {}
