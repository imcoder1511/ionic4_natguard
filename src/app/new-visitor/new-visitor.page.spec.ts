import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewVisitorPage } from './new-visitor.page';

describe('NewVisitorPage', () => {
  let component: NewVisitorPage;
  let fixture: ComponentFixture<NewVisitorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewVisitorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewVisitorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
