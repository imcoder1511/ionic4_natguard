import { Component, OnInit } from '@angular/core';
import { Camera } from '@ionic-native/camera/ngx';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ApiService } from '../service/api.service';
//import {TimeAgoPipe} from 'time-ago-pipe';
import * as jsPDF from 'jspdf'
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Util } from '../util/util';
declare var cordova: any;



@Component({
  selector: 'app-new-visitor',
  templateUrl: './new-visitor.page.html',
  styleUrls: ['./new-visitor.page.scss'],
})
export class NewVisitorPage implements OnInit {
  public visitorModel = {
    datetime: null,
    Name: "",
    ContactNo: "",
    VehicleNo: "",
    BuildingBlock: "",
    FlatNo: "",
    Reason: "",
    Picture: "",
    Date: "",
    Time: ""

  };

  public visitorData: any;
  public base64Image: string;
  public img_url: any;
  constructor(private camera: Camera,
    private platform: Platform,
    private router: Router,
    private storage: Storage,
    private file: File,
    private api_service: ApiService,
    private webview: WebView,
    private util: Util) {
    this.platform.backButton.subscribe(() => {
      this.router.navigate(['/listsimplevisitor']);
    });

  }

  ngOnInit() {
  }
  public createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  public copyFileToLocalDir(namePath, currentName, newFileName) {
    var newPath = "file:///storage/emulated/0/Android/data/io.ionic.visitors/Natguard_upload/";
    this.file.createDir("file:///storage/emulated/0/Android/data/io.ionic.visitors/", "Natguard_upload", false)
    this.file.copyFile(namePath, currentName, newPath, newFileName).then(success => {
      this.visitorModel.Picture = newPath + newFileName;

      var date = new Date();
      this.visitorModel.datetime = date;
      this.visitorModel.Date = date.toLocaleDateString();
      this.visitorModel.Time = date.toLocaleTimeString();
      this.storage.get("visitorData").then(val => {
        this.visitorData = [];
        if (JSON.parse(val) != null) {
          this.visitorData = JSON.parse(val);
        }
        this.visitorData.unshift(this.visitorModel);
        this.storage.set("visitorData", JSON.stringify(this.visitorData));

      })
      this.router.navigate(['/listsimplevisitor']);
    }, error => {
    });
  }

  public async takePicture() {
    this.img_url = await this.util.getFilePathFromCamera();
    this.base64Image = this.webview.convertFileSrc(this.img_url);
  }

  public submitVisitor() {
    //* Local Code *//
    if (this.img_url == undefined) {
      this.util.toastmethod("Please add image");
    } else {
      var currentName = this.img_url.substr(this.img_url.lastIndexOf('/') + 1);
      var correctPath = this.img_url.substr(0, this.img_url.lastIndexOf('/') + 1);
      this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    }
  }
}
