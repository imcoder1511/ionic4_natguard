import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Camera } from '@ionic-native/camera/ngx';
import { Platform, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ApiService } from '../service/api.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { File } from '@ionic-native/file/ngx';
import { Storage } from '@ionic/storage';
import { Util } from '../util/util';



@Component({
  selector: 'app-registerguard',
  templateUrl: './registerguard.page.html',
  styleUrls: ['./registerguard.page.scss'],
})
export class RegisterguardPage implements OnInit {
  public guardModel = {
    guardImage: "",
    Name: "",
    Password: "",
    SocietyName: "",
    Shift: "",
    // country:"",
    // state:""

  }
  countries = ['Afghanistan', 'Azerbaijan', 'Albania'];
  statesByCountry = {
    Afghanistan: ['AfghanA', 'AfghanB', 'AfghanC'],
    Azerbaijan: ['AzerA', 'AzerB', 'AzerC'],
    Albania: ['AlbaA', 'AlbaB', 'AlbaC'],
  };
  states = [];
  public guardData: any;
  public base64Image: string;
  public img_url: any;
  constructor(private camera: Camera,
    private platform: Platform,
    private router: Router,
    private storage: Storage,
    private file: File,
    private api_service: ApiService,
    private webview: WebView,
    private util: Util,
    private _cdr: ChangeDetectorRef) {
    this.platform.backButton.subscribe(() => {
      this.router.navigate(['/login']);
    });
  }

  ngOnInit() {
  }

  public createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  public copyFileToLocalDir(namePath, currentName, newFileName) {
    var newPath = "file:///storage/emulated/0/Android/data/io.ionic.visitors/Natguard_guard/";
    this.file.createDir("file:///storage/emulated/0/Android/data/io.ionic.visitors/", "Natguard_guard", false)
    this.file.copyFile(namePath, currentName, newPath, newFileName).then(success => {
      this.guardModel.guardImage = newPath + newFileName;
      this.storage.get("guardData").then(val => {
        this.guardData = [];
        if (JSON.parse(val) != null) {
          this.guardData = JSON.parse(val);
        }
        this.guardData.push(this.guardModel);
        this.storage.set("guardData", JSON.stringify(this.guardData));
      })
      this.router.navigate(['/login']);
    }, error => {
    });
  }

  public async takePicture() {
    this.img_url = await this.util.getFilePathFromCamera();
    this.base64Image = this.webview.convertFileSrc(this.img_url);
  }
  onCountryChange(): void {
    // let country =  this.guardModel.country;
    // this.states = this.statesByCountry[country];
    this._cdr.detectChanges();
  }

  public submitGuard() {
    //* Local Code *//
    if (this.img_url == undefined) {
      this.util.toastmethod("Please add image");
    } else {
      var currentName = this.img_url.substr(this.img_url.lastIndexOf('/') + 1);
      var correctPath = this.img_url.substr(0, this.img_url.lastIndexOf('/') + 1);
      this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    }
  }
}
