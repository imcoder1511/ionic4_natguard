import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
 // { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'newVisitor', loadChildren: './new-visitor/new-visitor.module#NewVisitorPageModule' },
  { path: 'visitors', loadChildren: './visitors/visitors.module#VisitorsPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'new Permenentvisitor', loadChildren: './new-permenentvisitor/new-permenentvisitor.module#NewPermenentvisitorPageModule' },
  { path: 'listPermenentvisitor', loadChildren: './list-permenentvisitor/list-permenentvisitor.module#ListPermenentvisitorPageModule' },
  { path: 'listvisitedpermenentvisitor', loadChildren: './listvisitedpermenentvisitor/listvisitedpermenentvisitor.module#ListvisitedpermenentvisitorPageModule' },
  { path: 'listsimplevisitor', loadChildren: './listsimplevisitor/listsimplevisitor.module#ListsimplevisitorPageModule' },
  { path: 'detailSimplevisitor', loadChildren: './detail-simplevisitor/detail-simplevisitor.module#DetailSimplevisitorPageModule' },
  { path: 'detailPermanentvisitor', loadChildren: './detail-permanentvisitor/detail-permanentvisitor.module#DetailPermanentvisitorPageModule' },
  { path: 'help', loadChildren: './help/help.module#HelpPageModule' },
  { path: 'registerguard', loadChildren: './registerguard/registerguard.module#RegisterguardPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
