import { Component, OnInit, NgZone } from '@angular/core';
import { Platform, AlertController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import * as jsPDF from 'jspdf'
require('jspdf-autotable');
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ApiService } from '../service/api.service';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Util } from '../util/util';




@Component({
  selector: 'app-listsimplevisitor',
  templateUrl: './listsimplevisitor.page.html',
  styleUrls: ['./listsimplevisitor.page.scss'],
})
export class ListsimplevisitorPage implements OnInit {
  public visitedList: any = [];
  public guardname = "";
  public base64image = "";
  public syncData: any = [];

  constructor(
    private platform: Platform,
    public util: Util,
    private router: Router,
    public storage: Storage,
    public file: File,
    public fileOpener: FileOpener,
    public webview: WebView,
    public alertController: AlertController,
    public apiService: ApiService,
    private zone: NgZone) {
    this.platform.backButton.subscribe(() => {
      this.zone.run(async () => {
        await this.router.navigate(['/visitors']);
      });
    });
    this.initlizeData();
  }

  ionViewWillEnter() {
    this.getLocalData();
    this.platform.backButton.subscribe(() => {
      this.router.navigateByUrl('/visitors');
    });


  }
  ionViewDidEnter() {
    this.getLocalData();
    this.platform.backButton.subscribe(() => {
      this.router.navigateByUrl('/visitors');
    });
  }

  public getLocalData() {
    this.storage.get("visitorData").then(val => {
      if (JSON.parse(val) != null) {
        this.visitedList = JSON.parse(val);
      }
    });
  }

  public initlizeData() {
    const directory = "file:///storage/emulated/0/Download/";
    const fileName = "Visitors.pdf";
    this.file.removeFile(directory, fileName);
    this.getLocalData();
  }

  public createPdf(that) {
    that.initlizeData();
    that.getLocalData();
    var gname;
    that.storage.get('loginUser').then((val) => {
      that.guardname = val;
      gname = that.guardname
      var columns = [
        { title: "Name", dataKey: "Name" },
        { title: "FlatNo", dataKey: "FlatNo" },
        { title: "VehicleNo", dataKey: "VehicleNo" },
        { title: "ContactNo", dataKey: "ContactNo" },
        { title: "Reason", dataKey: "Reason" },
        { title: "Date", dataKey: "Date" },
        { title: "Time", dataKey: "Time" },
      ];
      var rows = that.visitedList;
      var doc = new jsPDF('p', 'pt');
      doc.autoTable(columns, rows, {
        theme: 'striped',
        styles: { overflow: 'linebreak', columnWidth: 'wrap' },
        headerStyles: { fillColor: [73, 29, 0] },
        columnStyles: {
          Name: { columnWidth: 80 },
          VehicleNo: { columnWidth: 80 },
          Reason: { columnWidth: 80 }
        },
        margin: { top: 80 },
        addPageContent: function (data) {
          doc.setFontSize(30);
          doc.text("NatGuard", 40, 40);
          doc.setFontSize(15);
          doc.text("Guard Name:" + gname, 40, 70);
        }
      });
      doc.save('Visitors.pdf');

      let pdfOutput = doc.output();

      let buffer = new ArrayBuffer(pdfOutput.length);

      let array = new Uint8Array(buffer);

      for (var i = 0; i < pdfOutput.length; i++) {
        array[i] = pdfOutput.charCodeAt(i);
      }

      // For that, you have to use ionic native file plugin
      const directory = "file:///storage/emulated/0/Download/";

      const fileName = "Visitors.pdf";
      that.file.writeFile(directory, fileName, buffer)
        .then((success) => {
          that.fileOpener.open(directory + fileName, 'application/pdf');
        })
        .catch((error) => {
        });
      that.util.toastmethod('Report have been saved successfully.');
    });
  }

  public async savePDF() {
    var that = this;
    this.util.presentAlert('Save File!', '<strong>Your file stored in download folder</strong> <br/>Are you want to save your file?', () => { this.createPdf(that) })
  }

  ngOnInit() {
  }

  public detailStore(item) {
    this.apiService.currentsimplevisitor = item
    this.router.navigateByUrl('/detailSimplevisitor');
  }

}
