import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
// import {TimeAgoPipe} from 'time-ago-pipe';

import { IonicModule } from '@ionic/angular';

import { ListsimplevisitorPage } from './listsimplevisitor.page';
import { ListvisitedpermenentvisitorPageModule } from '../listvisitedpermenentvisitor/listvisitedpermenentvisitor.module';
import { SharedModule } from '../shared.module';

const routes: Routes = [
  {
    path: '',
    component: ListsimplevisitorPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    // ListvisitedpermenentvisitorPageModule
  ],
  declarations: [ListsimplevisitorPage],
  // exports:[TimeAgoPipe],

})
export class ListsimplevisitorPageModule {}
