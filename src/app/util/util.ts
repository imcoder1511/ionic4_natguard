import { Injectable } from '@angular/core';
import { AlertController, ToastController, LoadingController, PopoverController } from "@ionic/angular";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Injectable()
export class Util {

  public isLoading = false;
  constructor(
    public alertController: AlertController,
    public toastController: ToastController,
    public loadingController: LoadingController,
    public popoverController: PopoverController,
    public camera: Camera) {
  }
  // Active toast message
  public async toastmethod(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
  // active loading
  public async presentLoading(message) {
    this.isLoading = true;
    return await this.loadingController.create({
      duration: 5000,
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }
  //  dismiss loading
  public async dismissLoading() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }
  // active popover
  public async presentPopover(component) {
    const popover = await this.popoverController.create({
      component: component,
      event: event,
      translucent: true
    });
    return await popover.present();
  }
  // dismiss popover
  public async dismissPopover() {
    await this.popoverController.dismiss();
  }
  //Alert box
  public async presentAlert(header, message, success) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //       console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            //       console.log('Confirm Okay');
            success();
          }
        }
      ]
    });
    await alert.present();
  }
  // declare cameraOptions
  private cameraOptions: CameraOptions = {
    quality: 100,
    sourceType: this.camera.PictureSourceType.CAMERA,
    destinationType: this.camera.DestinationType.FILE_URI,
    correctOrientation: true,
    encodingType: this.camera.EncodingType.JPEG,
    cameraDirection: this.camera.Direction.FRONT,
  };

  // FilePathFromCamera
  public async getFilePathFromCamera() {
    const imagePath: string = await this.camera.getPicture(this.cameraOptions);
    return imagePath;
  }

}
