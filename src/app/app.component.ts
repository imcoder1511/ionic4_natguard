import { Component, ViewChild } from '@angular/core';

import { Platform, NavController} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { LoginPage } from './login/login.page';
import { VisitorsPage } from './visitors/visitors.page';
import { Router } from '@angular/router';
import * as jsPDF from 'jspdf'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  public guardData: any;
  public guard = {
    guardImage: "",
    Name: "Demo1",
    Password: "1234",
    SocietyName: "Demo Society",
    Shift: "Day"

  }

  public defaultVisitors =
   [{
    BuildingBlock: "B",
    ContactNo: "9595863254",
    Date: "14/03/2019",
    FlatNo: 304,
    Name: "Mukund",
    Picture: "assets/icon/user_avt.png",
    Reason: "Relative",
    Time: "18:12:53",
    VehicleNo: "GJ 01 AJ 2022",
    datetime: "2019-03-14T12:42:53.732Z",
  }, {
    BuildingBlock: "A",
    ContactNo: "9656325415",
    Date: "14/03/2019",
    FlatNo: 402,
    Name: "Prasoon",
    Picture: "assets/icon/user_avt.png",
    Reason: "Amazon delivery man",
    Time: "18:12:53",
    VehicleNo: "MH 04 BB 2038",
    datetime: "2019-03-14T12:42:53.732Z",
  }, {
    BuildingBlock: "C",
    ContactNo: "8655423514",
    Date: "14/03/2019",
    FlatNo: 304,
    Name: "Heena",
    Picture: "assets/icon/female_avt.png",
    Reason: "Food delivery girl",
    Time: "18:12:53",
    VehicleNo: "GJ 01 BA 8568",
    datetime: "2019-03-14T12:42:53.732Z",
  }, {
    BuildingBlock: "C",
    ContactNo: "8754213652",
    Date: "14/03/2019",
    FlatNo: 301,
    Name: "Mahadeo",
    Picture: "assets/icon/user_avt.png",
    Reason: "Relative",
    Time: "18:12:53",
    VehicleNo: "GJ 01 AB 5457",
    datetime: "2019-03-14T12:42:53.732Z",
  }, {
    BuildingBlock: "B",
    ContactNo: "8866323338",
    Date: "14/03/2019",
    FlatNo: 404,
    Name: "Kailash",
    Picture: "assets/icon/user_avt.png",
    Reason: "Amazon delivery man",
    Time: "18:12:53",
    VehicleNo: "GJ 04 YU 6767",
    datetime: "2019-03-14T12:42:53.732Z",
  }, {
    BuildingBlock: "E",
    ContactNo: "8654823651",
    Date: "14/03/2019",
    FlatNo: 506,
    Name: "Dinesh",
    Picture: "assets/icon/user_avt.png",
    Reason: "Flipkart delivery man",
    Time: "18:12:53",
    VehicleNo: "GJ 05 HK 7818",
    datetime: "2019-03-14T12:42:53.732Z",
  }, {
    BuildingBlock: "A",
    ContactNo: "6425621154",
    Date: "14/03/2019",
    FlatNo: 201,
    Name: "Jobin",
    Picture: "assets/icon/user_avt.png",
    Reason: "Amazon delivery man",
    Time: "18:12:53",
    VehicleNo: "GJ 09 GH 6545",
    datetime: "2019-03-14T12:42:53.732Z",
  }, {
    BuildingBlock: "E",
    ContactNo: "8856315235",
    Date: "14/03/2019",
    FlatNo: 602,
    Name: "Nitika",
    Picture: "assets/icon/female_avt.png",
    Reason: "Relative",
    Time: "18:12:53",
    VehicleNo: "GJ 08 LK 0089",
    datetime: "2019-03-14T12:42:53.732Z",
  }, {
    BuildingBlock: "A",
    ContactNo: "9896325142",
    Date: "14/03/2019",
    FlatNo: 701,
    Name: "Gowri",
    Picture: "assets/icon/female_avt.png",
    Reason: "Food delivey girl",
    Time: "18:12:53",
    VehicleNo: "GJ 01 AA 8978",
    datetime: "2019-03-14T12:42:53.732Z",
  }, {
    BuildingBlock: "B",
    ContactNo: "6632512458",
    Date: "14/03/2019",
    FlatNo: 303,
    Name: "Riya",
    Picture: "assets/icon/female_avt.png",
    Reason: "Relative",
    Time: "18:12:53",
    VehicleNo: "GJ 05 HJ 0007",
    datetime: "2019-03-14T12:42:53.732Z",
  }, {
    BuildingBlock: "C",
    ContactNo: "8866545215",
    Date: "14/03/2019",
    FlatNo: 101,
    Name: "Bishnu",
    Picture: "assets/icon/user_avt.png",
    Reason: "Amazon delivery man",
    Time: "18:12:53",
    VehicleNo: "GJ 05 JJ 9089",
    datetime: "2019-03-14T12:42:53.732Z",
  }, {
    BuildingBlock: "E",
    ContactNo: "8460166687",
    Date: "14/03/2019",
    FlatNo: 405,
    Name: "Lalit",
    Picture: "assets/icon/user_avt.png",
    Reason: "Flipkart delivery man",
    Time: "18:12:53",
    VehicleNo: "GJ 05 AH 8978",
    datetime: "2019-03-14T12:42:53.732Z",
  }, {
    BuildingBlock: "C",
    ContactNo: "8854216354",
    Date: "14/03/2019",
    FlatNo: 502,
    Name: "Raj",
    Picture: "assets/icon/user_avt.png",
    Reason: "Relative",
    Time: "18:12:53",
    VehicleNo: "GJ 05 LH 7656",
    datetime: "2019-03-14T12:42:53.732Z",
  }];

  public defaultPermenentvisitor = 
  [{
    contectno: "9898253535",
    flatno: "B-604,606,702,707, A-807,101,203,405,604",
    image: "assets/icon/user_avt.png",
    purpose: "Laundry man",
    visitorname: "Shashank"
  },
  {
    contectno: "9656854521",
    flatno: "B-101,501,603,402 C-205,206,304,305,401,402",
    image: "assets/icon/female_avt.png",
    purpose: "Maid",
    visitorname: "Richa"
  }, {
    contectno: "8654326584",
    flatno: "C-505",
    image: "assets/icon/user_avt.png",
    purpose: "Newspaper man",
    visitorname: "Emran"
  }, {
    contectno: "8899656325",
    flatno: "E-404,601,604,705",
    image: "assets/icon/female_avt.png",
    purpose: "Maid",
    visitorname: "Vaishali"
  }, {
    contectno: "9254125561",
    flatno: "A-404,E-405",
    image: "assets/icon/female_avt.png",
    purpose: "Maid",
    visitorname: "Pinky"
  }, {
    contectno: "7825321453",
    flatno: "D-501,604,304 E-401,403,402",
    image: "assets/icon/user_avt.png",
    purpose: "Newspaper man",
    visitorname: "Hari"
  }, {
    contectno: "9658521453",
    flatno: "A-404,608,305,701",
    image: "assets/icon/female_avt.png",
    purpose: "Maid",
    visitorname: "Aisha"
  }, {
    contectno: "8430251012",
    flatno: "E-402,604 A-205,602",
    image: "assets/icon/user_avt.png",
    purpose: "Laundry Man",
    visitorname: "Ricky"
  }, {
    contectno: "9898983254",
    flatno: "D-202, E-204,303,402,507",
    image: "assets/icon/user_avt.png",
    purpose: "Milk man",
    visitorname: "Govind"
  }, {
    contectno: "6535552314",
    flatno: "C-401,505,302,401 D-603,705,504",
    image: "assets/icon/user_avt.png",
    purpose: "Laundry Man",
    visitorname: "Satish"
  }, {
    contectno: "6354698654",
    flatno: "A-705,D-303,305",
    image: "assets/icon/female_avt.png",
    purpose: "Maid",
    visitorname: "Nupoor"
  }]

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public storage: Storage,
    public router: Router,
    public navCtrl: NavController,
  ) {
    this.initializeApp();

    //* Local Code *//
    this.storage.get("guardData").then(val => {
      if (val == null || val == "" || val == undefined) {
        this.guardData = [];
        if (JSON.parse(val) != null) {
          this.guardData = JSON.parse(val);
        }
        this.guardData.push(this.guard);
        this.storage.set("guardData", JSON.stringify(this.guardData));
      } 

    })
    this.storage.get("visitorData").then(val => {
      if (val == null || val == "" || val == undefined) {
        this.storage.set("visitorData", JSON.stringify(this.defaultVisitors));
      } 

    })
    this.storage.get("Permenentvisitor").then(val => {
      if (val == null || val == "" || val == undefined) {
        this.storage.set("Permenentvisitor", JSON.stringify(this.defaultPermenentvisitor));
      }

    })
    this.storage.get("loginUser").then((val) => {
      if (val == null || val == "" || val == undefined) {
        this.router.navigateByUrl('/login');
      } else {
        this.router.navigateByUrl('/visitors');
      }
    })
  }

  public initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleBlackOpaque();
      this.splashScreen.hide();
    });
  }
}
