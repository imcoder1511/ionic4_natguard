import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { NavController, Platform, LoadingController } from '@ionic/angular';
import { ApiService } from '../service/api.service';
import { Util } from '../util/util';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public checkUser: any;
  public checkPass: any;
  public checkShift: any;
  public subscription: any;
  public winHeight: any;
  public winWidth: any;
  public display: any;

  public user = {
    Name: "",
    Password: "",
    Shift: ""
  }
  public guardData = [];

  constructor(private router: Router,
    private storage: Storage,
    private util: Util,
    public platform: Platform,
    public api_service: ApiService,
    public loadingCtrl: LoadingController,
  ) {
    this.display = "block";

    // * Local Code *//

    this.storage.get('loginData').then((val) => {
      this.checkUser = JSON.parse(val);

    });
    this.storage.get('Password').then((val) => {
      this.checkPass = val;
    });
    this.storage.get('Shift').then((val) => {
      this.checkShift = val;
    });
  }
  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator['app'].exitApp();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }



  ngOnInit() {
    this.winHeight = window.screen.height;
    this.winWidth = window.screen.width;
    this.storage.get('display').then((val) => {
      this.display = val
    });
  }
  public hint_skip() {
    this.display = "none";
    this.storage.set('display', "none");

  }
  public defaultLogin() {
    this.storage.get("guardData").then(val => {
      this.guardData = JSON.parse(val);
      this.router.navigate(['/visitors']);
      this.storage.set('loginUser', this.guardData[0].Name);
      this.storage.set('societyName', this.guardData[0].SocietyName);

      this.storage.set('display', "none");
      this.display = "none";
    });

  }

  public async login() {
    if (this.user.Name == "") {
      this.util.toastmethod("UserName Required");
    } else if (this.user.Password == "") {
      this.util.toastmethod("Password Required");
    } else if (this.user.Shift == "") {
      this.util.toastmethod("Shift Required");
    } else {

      this.storage.get("guardData").then(val => {

        this.guardData = JSON.parse(val);
        for (let i in JSON.parse(val)) {
          if (this.user.Name.toLowerCase() == this.guardData[i].Name.toLowerCase() &&
            this.user.Name.toUpperCase() == this.guardData[i].Name.toUpperCase() &&
            this.user.Password == this.guardData[i].Password &&
            this.user.Shift.toLowerCase() == this.guardData[i].Shift.toLowerCase() &&
            this.user.Shift.toUpperCase() == this.guardData[i].Shift.toUpperCase()) {
            this.util.presentLoading('Please wait...')
            this.router.navigate(['/visitors']);
            this.storage.set('loginUser', this.guardData[i].Name);
            this.storage.set('societyName', this.guardData[i].SocietyName);
            this.storage.set('guardImage', this.guardData[i].guardImage);

          }
        }

        this.storage.get("loginUser").then(username => {
          if (username == "") {
            this.util.toastmethod("Contact to admin");
            this.util.dismissLoading();
          }
        })
      })
    }
  }
}
