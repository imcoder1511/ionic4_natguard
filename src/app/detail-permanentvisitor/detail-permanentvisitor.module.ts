import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetailPermanentvisitorPage } from './detail-permanentvisitor.page';

const routes: Routes = [
  {
    path: '',
    component: DetailPermanentvisitorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetailPermanentvisitorPage]
})
export class DetailPermanentvisitorPageModule {}
