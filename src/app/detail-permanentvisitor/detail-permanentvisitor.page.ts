import { Component, OnInit, NgZone } from '@angular/core';
import { ApiService } from '../service/api.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Platform } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { ListPermenentvisitorPage } from '../list-permenentvisitor/list-permenentvisitor.page';
import { Util } from '../util/util';

@Component({
  selector: 'app-detail-permanentvisitor',
  templateUrl: './detail-permanentvisitor.page.html',
  styleUrls: ['./detail-permanentvisitor.page.scss'],
})
export class DetailPermanentvisitorPage implements OnInit {

  public Pvisitordata: any;
  public button_flag: any;
  constructor(public apiService: ApiService,
    public webview: WebView,
    private platform: Platform,
    public util: Util,
    private router: Router,
    private zone: NgZone,
    private dd: ListPermenentvisitorPage,
    private activatedRoute: ActivatedRoute) {
    this.platform.backButton.subscribe(() => {
      this.zone.run(async () => {
        if (this.button_flag) {
          await this.router.navigate(['/listPermenentvisitor']);
        } else {
          await this.router.navigate(['/listvisitedpermenentvisitor']);
        }
      });
    });

  }

  ngOnInit() {
    this.Pvisitordata = this.apiService.currentpermanentvisitor;
    this.button_flag = this.activatedRoute.snapshot.paramMap.get('buttonFlag');
  }
  public onVisit() {
    this.dd.onVisit(this.Pvisitordata)
    this.util.toastmethod('Visitor have been visited successfully.');
    this.router.navigate(['/listPermenentvisitor']);

  }
}
