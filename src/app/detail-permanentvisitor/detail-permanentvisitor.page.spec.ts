import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPermanentvisitorPage } from './detail-permanentvisitor.page';

describe('DetailPermanentvisitorPage', () => {
  let component: DetailPermanentvisitorPage;
  let fixture: ComponentFixture<DetailPermanentvisitorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPermanentvisitorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPermanentvisitorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
