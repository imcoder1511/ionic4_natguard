import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';


import { IonicModule } from '@ionic/angular';

import { ListPermenentvisitorPage } from './list-permenentvisitor.page';

const routes: Routes = [
  {
    path: '',
    component: ListPermenentvisitorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListPermenentvisitorPage]
})
export class ListPermenentvisitorPageModule {}
