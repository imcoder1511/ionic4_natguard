import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ApiService } from '../service/api.service';
import { Util } from '../util/util';

@Component({
  selector: 'app-list-permenentvisitor',
  templateUrl: './list-permenentvisitor.page.html',
  styleUrls: ['./list-permenentvisitor.page.scss'],
})
export class ListPermenentvisitorPage implements OnInit {
  public items: any = [];
  public visited: any;
  public filterData: any = [];
  public search: boolean = false;
  constructor(public platform: Platform,
    public router: Router,
    public util: Util,
    public storage: Storage,
    public webview: WebView,
    public apiService: ApiService) {
    this.initlizeData();
    this.platform.backButton.subscribe(() => {
      this.router.navigate(['/visitors']);
    });
  }

  ionViewWillEnter() {
    this.platform.backButton.subscribe(() => {
      this.router.navigateByUrl('/visitors');
    });


  }
  ionViewDidEnter() {
    this.initlizeData();
    this.platform.backButton.subscribe(() => {
      this.router.navigateByUrl('/visitors');
    });
  }

  ngOnInit() {
  }

  public initlizeData() {
    this.storage.get("Permenentvisitor").then(val => {
      if (JSON.parse(val) != null) {
        this.search = false;
        this.items = JSON.parse(val);
      }
    });
  }

  public getItems(ev) {
    this.search = true
    let val = ev.detail.data;
    if (val && val.trim() != '') {
      this.filterData = this.items.filter(item => {
        return item.visitorname.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
    } else {
      this.initlizeData();
    }
  }

  public onVisit(item) {
    var date = new Date();
    item.Date = date.toLocaleDateString();
    item.Time = date.toLocaleTimeString();
    item.datetime = date;

    this.storage.get("Visited").then(val => {
      this.visited = [];
      if (JSON.parse(val) != null) {
        this.visited = JSON.parse(val);
      }
      this.visited.unshift(item);
      this.storage.set("Visited", JSON.stringify(this.visited));
      this.util.toastmethod('Visitor have been visited successfully.');
    })
  }
  public onDetails(item) {
    this.apiService.currentpermanentvisitor = item;
    this.router.navigate(['/detailPermanentvisitor', { buttonFlag: true }]);
  }
}
