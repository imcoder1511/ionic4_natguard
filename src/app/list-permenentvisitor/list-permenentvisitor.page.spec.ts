import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPermenentvisitorPage } from './list-permenentvisitor.page';

describe('ListPermenentvisitorPage', () => {
  let component: ListPermenentvisitorPage;
  let fixture: ComponentFixture<ListPermenentvisitorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPermenentvisitorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPermenentvisitorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
