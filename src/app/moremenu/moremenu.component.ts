import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { AppVersion } from "@ionic-native/app-version/ngx";
import { Util } from '../util/util';



@Component({
  selector: 'app-moremenu',
  templateUrl: './moremenu.component.html',
  styleUrls: ['./moremenu.component.scss']
})
export class MoremenuComponent implements OnInit {
  public version: any;
  public Logindata = [
    {
      UserName: "Ramesh",
      Password: "123456",
      Shift: "Day"
    },
    {
      UserName: "Mahesh",
      Password: "123456",
      Shift: "Noon"
    },
    {
      UserName: "Hiren",
      Password: "123456",
      Shift: "Night"
    }
  ]
  constructor(
    private router: Router,
    public storage: Storage,
    private util:Util,
    public popoverController: PopoverController,
    public appVersion: AppVersion

  ) {
    this.appVersion.getVersionNumber().then(data => {
      this.version = data;
    }, error => {
    });

  }
  public permenentVisitor() {
   this.util.dismissPopover();
    this.router.navigate(["/listPermenentvisitor"]);
  }
  public offship() {
    this.storage.set("loginUser", "");
    this.util.dismissPopover();
    this.router.navigate(["/login"]);
  }

  public help() {
   this.util.dismissPopover();
    this.router.navigate(["/help"]);
  }

  ngOnInit() {
  }

}
