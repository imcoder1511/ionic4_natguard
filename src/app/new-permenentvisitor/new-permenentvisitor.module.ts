import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NewPermenentvisitorPage } from './new-permenentvisitor.page';

const routes: Routes = [
  {
    path: '',
    component: NewPermenentvisitorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NewPermenentvisitorPage]
})
export class NewPermenentvisitorPageModule {}
