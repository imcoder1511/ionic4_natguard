import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { Camera } from '@ionic-native/camera/ngx';
import { Storage } from '@ionic/storage';
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Util } from '../util/util';

@Component({
  selector: 'app-new-permenentvisitor',
  templateUrl: './new-permenentvisitor.page.html',
  styleUrls: ['./new-permenentvisitor.page.scss'],
})
export class NewPermenentvisitorPage implements OnInit {

  public permenentVisitormodel = {
    contectno: "",
    flatno: "",
    purpose: "",
    visitorname: "",
    image: ""
  };
  public base64Image: string;
  public permenentVisitorData: any;
  img_url: any;
  constructor(private camera: Camera,
    private platform: Platform,
    private router: Router,
    private storage: Storage,
    private webview: WebView,
    private file: File,
    private util: Util) {
    this.platform.backButton.subscribe(() => {
      this.router.navigate(['/listPermenentvisitor']);
    });
  }

  ngOnInit() {
  }

  public createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  public async takePicture() {
    this.img_url = await this.util.getFilePathFromCamera();
    this.base64Image = this.webview.convertFileSrc(this.img_url);
  }

  public submitPvisitor() {
    //this.visitor.image=this.base64Image
    if (this.img_url == undefined) {
      this.util.toastmethod("Please add image");
    } else {
      var currentName = this.img_url.substr(this.img_url.lastIndexOf('/') + 1);
      var correctPath = this.img_url.substr(0, this.img_url.lastIndexOf('/') + 1);
      this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    }
  }

  public copyFileToLocalDir(namePath, currentName, newFileName) {
    var newPath = "file:///storage/emulated/0/Android/data/io.ionic.visitors/Natguard_Permenent_upload/";
    this.file.createDir("file:///storage/emulated/0/Android/data/io.ionic.visitors/", "Natguard_Permenent_upload", false)
    this.file.copyFile(namePath, currentName, newPath, newFileName).then(success => {
      this.permenentVisitormodel.image = newPath + newFileName;
      this.storage.get("Permenentvisitor").then(val => {
        this.permenentVisitorData = [];
        if (JSON.parse(val) != null) {
          this.permenentVisitorData = JSON.parse(val);
        }
        this.permenentVisitorData.unshift(this.permenentVisitormodel);
        this.storage.set("Permenentvisitor", JSON.stringify(this.permenentVisitorData));
      })
      this.router.navigate(['/listPermenentvisitor']);
    }, error => {
    });
  }
}
