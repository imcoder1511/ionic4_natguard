import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPermenentvisitorPage } from './new-permenentvisitor.page';

describe('NewPermenentvisitorPage', () => {
  let component: NewPermenentvisitorPage;
  let fixture: ComponentFixture<NewPermenentvisitorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPermenentvisitorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPermenentvisitorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
