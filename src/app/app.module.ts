import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, RouteReuseStrategy, Routes } from '@angular/router';
import { AppVersion } from "@ionic-native/app-version/ngx";

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera/ngx';
import { MoremenuComponent } from './moremenu/moremenu.component';
import { SharedModule } from './shared.module';
// import { HttpClientModule } from '@angular/common/http';
import { HttpProvider } from './service/http';
import { HttpModule } from '@angular/http';
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import { ListPermenentvisitorPage } from './list-permenentvisitor/list-permenentvisitor.page';
import { Util } from './util/util';



@NgModule({
  declarations: [AppComponent, MoremenuComponent],
  entryComponents: [MoremenuComponent],
  imports: [
    SharedModule,
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    HttpModule
    // HttpClientModule
    
  ],
  exports: [ MoremenuComponent ],
  providers: [
    Util,
    Camera,
    File,
    FileOpener,
    WebView,
    StatusBar,
    HttpProvider,
    AppVersion,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    ListPermenentvisitorPage
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
