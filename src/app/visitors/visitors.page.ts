import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { MoremenuComponent } from '../moremenu/moremenu.component';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Util } from '../util/util';


@Component({
  selector: 'app-visitors',
  templateUrl: './visitors.page.html',
  styleUrls: ['./visitors.page.scss'],
})
export class VisitorsPage implements OnInit {
  public guardName: any;
  public societyName: any;
  public guardImage: any;
  public subscription: any;
  constructor(
    private util: Util,
    private router: Router,
    private platform: Platform,
    public storage: Storage,
    public popoverController: PopoverController,
    private webview: WebView,
  ) {
    this.util.dismissLoading();
  }

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator['app'].exitApp();
    });
  }
  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }
  ionViewWillEnter() {
    this.storage.get('loginUser').then((val) => {
      this.guardName = val;
    });
    this.storage.get('societyName').then((val) => {
      this.societyName = val;
    });
    this.storage.get('guardImage').then((val) => {
      if (val != null || val != "" || val != undefined) {
        this.guardImage = this.webview.convertFileSrc(val);
      } else {
        this.guardImage = "assets/icon/if_profle_1055000.png"
      }
    });
  }
  ngOnInit() {
  }
  public pushVisitor() {
    this.router.navigate(['/listsimplevisitor']);
  }
  public clearls() {
    this.storage.clear();
  }
  public async presentPopover(ev: any) {
    this.util.presentPopover(MoremenuComponent);
  }

}
