import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-help',
  templateUrl: './help.page.html',
  styleUrls: ['./help.page.scss'],
})
export class HelpPage implements OnInit {

  constructor(
    private platform: Platform,
    private router: Router) {
    this.platform.backButton.subscribe(() => {
      this.router.navigateByUrl('/visitors');
    });
  }

  ngOnInit() {
  }

}
