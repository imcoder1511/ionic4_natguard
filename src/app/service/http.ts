import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
// import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import 'rxjs/add/operator/catch';
@Injectable()
export class HttpProvider {
  options: any;
  headers: Headers;
  constructor(
    private http: Http) {
  }
//   addHeader(headerName: string, headerValue: string) {
//     if (!this.headers) {
//       this.headers = new Headers();
//     }
//     (this.headers).set(headerName, headerValue);
//   }
  get(url) {
    return this.http.get(url, { headers: this.headers }).map(res => res.json()).catch(this.handleError).do(data => {
    }, error => {
    });
  }
  delete(url) {
    return this.http.delete(url, { headers: this.headers }).map(res => res.json()).catch(this.handleError).do(data => {
    }, error => {
    });
  }
  post(url, data) {
    
    return this.http.post(url, data, { headers: this.headers }).map(res => res.json()).catch(this.handleError).do(data => {
    
    }, error => {
    });
  }
  private handleError(error) {
    return Observable.throw(error.json().error || 'Server error');
  }
}