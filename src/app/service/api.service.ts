import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
// import { HttpClient } from '@angular/common/http';
// import { HTTP } from '@ionic-native/http';
import { HttpProvider } from './http';
const API_URL= environment.api_url;

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpclient: HttpProvider) { }
  currentsimplevisitor:any;
  currentpermanentvisitor:any;
  headers:Headers

  testApi(data){
    this.httpclient.post("https://jsonplaceholder.typicode.com/posts",data).subscribe(data => {
    }, err => {
    });
  }

  syncVisitor(syncData){
 //  this.httpclient.addHeader("Content-Type", "application/json");
   return this.httpclient.post(API_URL+"Visitor/PostTempVisitor",syncData).subscribe(data => {
    }, err => {
    });
      
   
  }

  addVisitor(Visitordata){
    return this.httpclient.post(API_URL+"Visitor/CreateVisitor",Visitordata);
  }

  listVisitor(){
    return this.httpclient.get("http://192.168.0.125:1050/api/Project/GetAllClients");
  }

  // addPermenentvisitor(data){
  //   this.httpclient.post(API_URL+"",data);
  // }

  // listPermenentvisitor(){
  //   return this.httpclient.get(API_URL+"");
  // }

  // visitedPermenentvisitor(data){
  //   this.httpclient.post(API_URL+"",data);
  // }

  login(userdata){
    return this.httpclient.post(API_URL+"Account/Login",userdata);
  }
}
